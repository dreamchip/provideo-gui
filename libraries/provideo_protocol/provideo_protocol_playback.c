/*
 * Copyright (C) 2017 Dream Chip Technologies GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
/**
 * @file    provideo_protocol_playback.c
 *
 * @brief   Implementation of playback- and record engine command functions
 *          for a provideo host controller.
 *
 *****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <ctrl_channel/ctrl_channel.h>

#include <ctrl_protocol/ctrl_protocol.h>
#include <ctrl_protocol/ctrl_protocol_playback.h>

#include <provideo_protocol/provideo_protocol_common.h>

/**
 * @brief command "rec"
 *****************************************************************************/
#define CMD_GET_REC ("rec\n")
#define CMD_SET_REC ("rec %i\n")
#define CMD_SYNC_REC ("rec ")
#define CMD_REC_NO_PARMS (1)

/**
 * @brief command "rec_mode"
 *****************************************************************************/
#define CMD_GET_REC_MODE ("rec_mode\n")
#define CMD_SET_REC_MODE ("rec_mode %i\n")
#define CMD_SYNC_REC_MODE ("rec_mode ")
#define CMD_REC_MODE_NO_PARMS (1)

/**
 * @brief command "rec_stop"
 *****************************************************************************/
#define CMD_REC_STOP ("rec_stop\n")

/**
 * @brief command "play"
 *****************************************************************************/
#define CMD_GET_PLAY ("play\n")
#define CMD_SET_PLAY ("play %i %i\n")
#define CMD_SYNC_PLAY ("play ")
#define CMD_PLAY_NO_PARMS (2)

/**
 * @brief command "play_mode"
 *****************************************************************************/
#define CMD_GET_PLAY_MODE ("play_mode\n")
#define CMD_SET_PLAY_MODE ("play_mode %i\n")
#define CMD_SYNC_PLAY_MODE ("play_mode ")
#define CMD_PLAY_MODE_NO_PARMS (1)

/**
 * @brief command "pause"
 *****************************************************************************/
#define CMD_PAUSE ("pause\n")

/**
 * @brief command "stop"
 *****************************************************************************/
#define CMD_STOP ("stop\n")

/**
 * @brief command "stop_mode"
 *****************************************************************************/
#define CMD_GET_STOP_MODE ("stop_mode\n")
#define CMD_SET_STOP_MODE ("stop_mode %i\n")
#define CMD_SYNC_STOP_MODE ("stop_mode ")
#define CMD_STOP_MODE_NO_PARMS (1)

/**
 * @brief command "pos"
 *****************************************************************************/
#define CMD_GET_POS ("pos\n")
#define CMD_POS_RESPONSE ("pos %i\n")
#define CMD_SYNC_POS ("pos ")
#define CMD_POS_NO_PARMS (1)

/**
 * @brief command "seek"
 *****************************************************************************/
#define CMD_SET_SEEK ("seek %i %i\n")
#define CMD_SEEK_NO_PARMS (2)

/**
 * @brief command "mark_in"
 *****************************************************************************/
#define CMD_SET_MARK_IN ("mark_in %i\n")
#define CMD_MARK_IN_NO_PARMS (1)

/**
 * @brief command "mark_out"
 *****************************************************************************/
#define CMD_SET_MARK_OUT ("mark_out %i\n")
#define CMD_MARK_OUT_NO_PARMS (1)

/**
 * @brief command "mark_pos"
 *****************************************************************************/
#define CMD_GET_MARK_POS ("mark_pos\n")
#define CMD_MARK_POS_RESPONSE ("mark_pos %i %i\n")
#define CMD_SYNC_MARK_POS ("mark_pos ")
#define CMD_MARK_POS_NO_PARMS (2)

/**
 * @brief command "status"
 *****************************************************************************/
#define CMD_GET_STATUS ("status %i\n")
#define CMD_GET_STATUS_NO_PARMS (1)
#define CMD_STATUS_RESPONSE ("B%i: %i / %i %s\n")
#define CMD_SYNC_STATUS_RESPONSE ("B")
#define CMD_STATUS_RESPONSE_NO_PARMS (4)

/**
 * @brief command "free"
 *****************************************************************************/
#define CMD_SET_FREE ("free %i\n")
#define CMD_SET_FREE_TMO (1000)

/**
 * @brief command "count"
 *****************************************************************************/
#define CMD_GET_COUNT ("count\n")
#define CMD_SET_COUNT ("count %i\n")
#define CMD_SYNC_COUNT ("count ")
#define CMD_COUNT_NO_PARMS (1)
#define CMD_SET_COUNT_TMO (15000)

/**
 * @brief command "health"
 *****************************************************************************/
#define CMD_GET_HEALTH ("health\n")
#define CMD_HEALTH_RESPONSE ("SSD%hhd: H %hhd(%[^)]), T %hhd/%hhd/%hhd, L %hhd\n")
#define CMD_SYNC_HEALTH ("SSD")
#define CMD_HEALTH_NO_PARMS (7)
#define CMD_GET_HEALTH_TMO (1000)

/*
 * get_rec
 *****************************************************************************/
static int get_rec(void *const ctx, ctrl_channel_handle_t channel, uint8_t *const id)
{
    (void)ctx;

    // parameter check
    if (!id) {
        return (-EINVAL);
    }

    // command call to get 1 parameter from provideo system
    int value = 0;
    int res = get_param_int_X(channel, 2, CMD_GET_REC, CMD_SYNC_REC, CMD_SET_REC, &value);

    // return error code
    if (res < 0) {
        return (res);
    }

    // return -EFAULT if number of parameter not matching
    else if (res != CMD_REC_NO_PARMS) {
        return (-EFAULT);
    }

    // type-cast to range
    *id = UINT8(value);

    return (0);
}

/*
 * set_rec
 *****************************************************************************/
static int set_rec(void *const ctx, ctrl_channel_handle_t channel, uint8_t const id)
{
    (void)ctx;

    return (set_param_int_X(channel, CMD_SET_REC, INT(id)));
}

/*
 * get_rec_mode
 *****************************************************************************/
static int get_rec_mode(void *const ctx, ctrl_channel_handle_t channel, uint8_t *const mode)
{
    (void)ctx;

    // parameter check
    if (!mode) {
        return (-EINVAL);
    }

    // command call to get 1 parameter from provideo system
    int value = 0;
    int res = get_param_int_X(channel, 2, CMD_GET_REC_MODE, CMD_SYNC_REC_MODE, CMD_SET_REC_MODE,
                              &value);

    // return error code
    if (res < 0) {
        return (res);
    }

    // return -EFAULT if number of parameter not matching
    else if (res != CMD_REC_MODE_NO_PARMS) {
        return (-EFAULT);
    }

    // type-cast to range
    *mode = UINT8(value);

    return (0);
}

/*
 * set_rec_mode
 *****************************************************************************/
static int set_rec_mode(void *const ctx, ctrl_channel_handle_t channel, uint8_t const mode)
{
    (void)ctx;

    return (set_param_int_X(channel, CMD_SET_REC_MODE, INT(mode)));
}

/*
 * rec_stop
 *****************************************************************************/
static int rec_stop(void *const ctx, ctrl_channel_handle_t channel)
{
    (void)ctx;

    return (set_param_0(channel, CMD_REC_STOP));
}

/*
 * get_play
 *****************************************************************************/
static int get_play(void *const ctx, ctrl_channel_handle_t channel, int const no,
                    int16_t *const values)
{
    (void)ctx;

    // parameter check
    if (no != CMD_PLAY_NO_PARMS || !values) {
        return (-EINVAL);
    }

    // command call to get 2 parameters from provideo system
    int id = 0;
    int speed = 0;
    int res = get_param_int_X(channel, 2, CMD_GET_PLAY, CMD_SYNC_PLAY, CMD_SET_PLAY, &id, &speed);

    // return error code
    if (res < 0) {
        return (res);
    }

    // return -EFAULT if number of parameter not matching
    else if (res != CMD_PLAY_NO_PARMS) {
        return (-EFAULT);
    }

    // type-cast to range
    values[0] = INT16(id);
    values[1] = INT16(speed);

    return (0);
}

/*
 * set_play
 *****************************************************************************/
static int set_play(void *const ctx, ctrl_channel_handle_t channel, int const no,
                    int16_t *const values)
{
    (void)ctx;

    if (no != CMD_PLAY_NO_PARMS) {
        return (-EFAULT);
    }

    return (set_param_int_X(channel, CMD_SET_PLAY, INT(values[0]), INT(values[1])));
}

/*
 * get_play_mode
 *****************************************************************************/
static int get_play_mode(void *const ctx, ctrl_channel_handle_t channel, uint8_t *const mode)
{
    (void)ctx;

    // parameter check
    if (!mode) {
        return (-EINVAL);
    }

    // command call to get 1 parameter from provideo system
    int value = 0;
    int res = get_param_int_X(channel, 2, CMD_GET_PLAY_MODE, CMD_SYNC_PLAY_MODE, CMD_SET_PLAY_MODE,
                              &value);

    // return error code
    if (res < 0) {
        return (res);
    }

    // return -EFAULT if number of parameter not matching
    else if (res != CMD_PLAY_MODE_NO_PARMS) {
        return (-EFAULT);
    }

    // type-cast to range
    *mode = UINT8(value);

    return (0);
}

/*
 * set_play_mode
 *****************************************************************************/
static int set_play_mode(void *const ctx, ctrl_channel_handle_t channel, uint8_t const mode)
{
    (void)ctx;

    return (set_param_int_X(channel, CMD_SET_PLAY_MODE, INT(mode)));
}

/*
 * pause
 *****************************************************************************/
static int pause(void *const ctx, ctrl_channel_handle_t channel)
{
    (void)ctx;

    return (set_param_0(channel, CMD_PAUSE));
}

/*
 * stop
 *****************************************************************************/
static int stop(void *const ctx, ctrl_channel_handle_t channel)
{
    (void)ctx;

    return (set_param_0(channel, CMD_STOP));
}

/*
 * get_stop_mode
 *****************************************************************************/
static int get_stop_mode(void *const ctx, ctrl_channel_handle_t channel, uint8_t *const mode)
{
    (void)ctx;

    // parameter check
    if (!mode) {
        return (-EINVAL);
    }

    // command call to get 1 parameter from provideo system
    int value = 0;
    int res = get_param_int_X(channel, 2, CMD_GET_STOP_MODE, CMD_SYNC_STOP_MODE, CMD_SET_STOP_MODE,
                              &value);

    // return error code
    if (res < 0) {
        return (res);
    }

    // return -EFAULT if number of parameter not matching
    else if (res != CMD_STOP_MODE_NO_PARMS) {
        return (-EFAULT);
    }

    // type-cast to range
    *mode = UINT8(value);

    return (0);
}

/*
 * set_stop_mode
 *****************************************************************************/
static int set_stop_mode(void *const ctx, ctrl_channel_handle_t channel, uint8_t const mode)
{
    (void)ctx;

    return (set_param_int_X(channel, CMD_SET_STOP_MODE, INT(mode)));
}

/*
 * set_seek
 *****************************************************************************/
static int set_seek(void *const ctx, ctrl_channel_handle_t channel, int const no,
                    int32_t *const values)
{
    (void)ctx;

    if (no != CMD_SEEK_NO_PARMS) {
        return (-EFAULT);
    }

    return (set_param_int_X(channel, CMD_SET_SEEK, INT(values[0]), INT(values[1])));
}

/*
 * get_pos
 *****************************************************************************/
static int get_pos(void *const ctx, ctrl_channel_handle_t channel, uint32_t *const pos)
{
    (void)ctx;

    // parameter check
    if (!pos) {
        return (-EINVAL);
    }

    // command call to get 1 parameter from provideo system
    int value = 0;
    int res = get_param_int_X(channel, 2, CMD_GET_POS, CMD_SYNC_POS, CMD_POS_RESPONSE, &value);

    // return error code
    if (res < 0) {
        return (res);
    }

    // return -EFAULT if number of parameter not matching
    else if (res != CMD_POS_NO_PARMS) {
        return (-EFAULT);
    }

    // type-cast to range
    *pos = UINT32(value);

    return (0);
}

/*
 * mark_in
 *****************************************************************************/
static int mark_in(void *const ctx, ctrl_channel_handle_t channel, uint32_t const pos)
{
    (void)ctx;

    return (set_param_int_X(channel, CMD_SET_MARK_IN, INT(pos)));
}

/*
 * mark_out
 *****************************************************************************/
static int mark_out(void *const ctx, ctrl_channel_handle_t channel, uint32_t const pos)
{
    (void)ctx;

    return (set_param_int_X(channel, CMD_SET_MARK_OUT, INT(pos)));
}

/*
 * get_mark_pos
 *****************************************************************************/
static int get_mark_pos(void *const ctx, ctrl_channel_handle_t channel, int const no,
                        uint32_t *const values)
{
    (void)ctx;

    // parameter check
    if (no != CMD_PLAY_NO_PARMS || !values) {
        return (-EINVAL);
    }

    // command call to get 2 parameters from provideo system
    int mark_in = 0;
    int mark_out = 0;
    int res = get_param_int_X(channel, 2, CMD_GET_MARK_POS, CMD_SYNC_MARK_POS,
                              CMD_MARK_POS_RESPONSE, &mark_in, &mark_out);

    // return error code
    if (res < 0) {
        return (res);
    }

    // return -EFAULT if number of parameter not matching
    else if (res != CMD_PLAY_NO_PARMS) {
        return (-EFAULT);
    }

    // type-cast to range
    values[0] = UINT32(mark_in);
    values[1] = UINT32(mark_out);

    return (0);
}

/*
 * get_status
 *****************************************************************************/
static int get_status(void *const ctx, ctrl_channel_handle_t channel, int const no,
                      uint8_t *const values)
{
    (void)ctx;

    char command[CMD_SINGLE_LINE_COMMAND_SIZE];
    char data[CMD_SINGLE_LINE_RESPONSE_SIZE];

    // parameter check
    if (no != sizeof(ctrl_protocol_buffer_info_t) || !values) {
        return (-EINVAL);
    }

    // get pointer to buffer_info struct and extract buffer_id
    ctrl_protocol_buffer_info_t *buffer_info = (ctrl_protocol_buffer_info_t *)values;
    uint32_t buffer_id = buffer_info->buffer_id;

    // clear command buffer
    memset(command, 0, sizeof(command)); // NOLINT

    // create command to send
    int res = sprintf(command, CMD_GET_STATUS, buffer_id); // NOLINT

    // prevent buffer overrun
    if (res >= INT(sizeof(command))) {
        return (-EFAULT);
    }

    // send command to COM port
    ctrl_channel_send_request(channel, (uint8_t *)command, INT(strlen(command)));

    // read response from provideo device
    res = evaluate_get_response(channel, data, sizeof(data));
    if (!res) {
        // set pointer to start of buffer info string
        char *s = strstr(data, CMD_SYNC_STATUS_RESPONSE);
        if (s) {
            // read buffer info
            res = sscanf(s, CMD_STATUS_RESPONSE, &buffer_info->buffer_id, // NOLINT
                         &buffer_info->num_frames, &buffer_info->max_frames, buffer_info->status);
            if (res != CMD_STATUS_RESPONSE_NO_PARMS) {
                return (-EFAULT);
            }

            // check buffer info for consistency (buffer_id has to match)
            if (buffer_id != buffer_info->buffer_id) {
                return (-EFAULT);
            }
        }
    } else {
        return evaluate_error_response(data, res);
    }

    return (0);
}

/*
 * set_free
 *****************************************************************************/
static int set_free(void *const ctx, ctrl_channel_handle_t channel, uint8_t const id)
{
    (void)ctx;

    return (set_param_int_X_with_tmo(channel, CMD_SET_FREE, CMD_SET_FREE_TMO, INT(id)));
}

/*
 * get_count
 *****************************************************************************/
static int get_count(void *const ctx, ctrl_channel_handle_t channel, uint8_t *const num)
{
    (void)ctx;

    // parameter check
    if (!num) {
        return (-EINVAL);
    }

    // command call to get 1 parameter from provideo system
    int value = 0;
    int res = get_param_int_X(channel, 2, CMD_GET_COUNT, CMD_SYNC_COUNT, CMD_SET_COUNT, &value);

    // return error code
    if (res < 0) {
        return (res);
    }

    // return -EFAULT if number of parameter not matching
    else if (res != CMD_COUNT_NO_PARMS) {
        return (-EFAULT);
    }

    // type-cast to range
    *num = UINT8(value);

    return (0);
}

/*
 * set_count
 *****************************************************************************/
static int set_count(void *const ctx, ctrl_channel_handle_t channel, uint8_t const num)
{
    (void)ctx;

    return (set_param_int_X_with_tmo(channel, CMD_SET_COUNT, CMD_SET_COUNT_TMO, INT(num)));
}

/*
 * get_health
 *****************************************************************************/
static int get_health(void *const ctx, ctrl_channel_handle_t channel, int const no,
                      uint8_t *const values)
{
    (void)ctx;

    char data[2 * CMD_SINGLE_LINE_RESPONSE_SIZE];

    // parameter check
    if (no != 2 * sizeof(ctrl_protocol_ssd_status_t) || !values) {
        return (-EINVAL);
    }

    // get pointer to ssd_status struct
    ctrl_protocol_ssd_status_t *ssd_status = (ctrl_protocol_ssd_status_t *)values;

    // send command to COM port
    ctrl_channel_send_request(channel, (uint8_t *)CMD_GET_HEALTH, INT(strlen(CMD_GET_HEALTH)));

    // read response from provideo device
    int res = evaluate_get_response_with_tmo(channel, data, sizeof(data), CMD_GET_HEALTH_TMO);
    if (!res) {
        // loop over both SSDs
        char *data_ptr = data;
        for (int i = 0; i < 2; i++) {
            // clear status string
            memset(ssd_status[i].status_string, 0, sizeof(ssd_status[i].status_string)); // NOLINT

            // set pointer to start of buffer info string
            char *s = strstr(data_ptr, CMD_SYNC_HEALTH);
            if (s) {
                // read buffer info
                res = sscanf(s, CMD_HEALTH_RESPONSE, &ssd_status[i].ssd_id, // NOLINT
                             &ssd_status[i].status_code, ssd_status[i].status_string,
                             &ssd_status[i].temp_min, &ssd_status[i].temp, &ssd_status[i].temp_max,
                             &ssd_status[i].lifetime);
                if (res != CMD_HEALTH_NO_PARMS) {
                    return (-EFAULT);
                }
            }

            // Advance data pointer behind first sync word to find second one in next iteration
            data_ptr += strlen(CMD_SYNC_HEALTH);
        }
    } else {
        return evaluate_error_response(data, res);
    }

    return (0);
}

/*
 * Playback protocol driver declaration
 *****************************************************************************/
static ctrl_protocol_playback_drv_t provideo_playback_drv = {
    // record control
    .get_rec = get_rec,
    .set_rec = set_rec,
    .get_rec_mode = get_rec_mode,
    .set_rec_mode = set_rec_mode,
    .rec_stop = rec_stop,
    // playback control
    .get_play = get_play,
    .set_play = set_play,
    .get_play_mode = get_play_mode,
    .set_play_mode = set_play_mode,
    .pause = pause,
    .stop = stop,
    .get_stop_mode = get_stop_mode,
    .set_stop_mode = set_stop_mode,
    .set_seek = set_seek,
    .get_pos = get_pos,
    .mark_in = mark_in,
    .mark_out = mark_out,
    .get_mark_pos = get_mark_pos,
    // buffer control
    .get_status = get_status,
    .set_free = set_free,
    .get_count = get_count,
    .set_count = set_count,
    .get_health = get_health
};

/*
 * provideo_protocol_playback_init
 *****************************************************************************/
int provideo_protocol_playback_init(ctrl_protocol_handle_t handle, void *const ctx)
{
    return (ctrl_protocol_playback_register(handle, ctx, &provideo_playback_drv));
}
