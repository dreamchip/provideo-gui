/*
 * Copyright (C) 2019 Dream Chip Technologies GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
/**
 * @file    provideo_protocol_lens.c
 *
 * @brief   Implementation of provideo protocol lens functions
 *
 *****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <errno.h>

#include <ctrl_channel/ctrl_channel.h>

#include <ctrl_protocol/ctrl_protocol.h>
#include <ctrl_protocol/ctrl_protocol_lens.h>

#include <provideo_protocol/provideo_protocol_common.h>

#define DEFAULT_RANGE (100)
#define DEFAULT_FINE_RANGE (1000)

/**
 * @brief command "lens_setup"
 *****************************************************************************/
#define CMD_GET_LENS_SETTINGS ("lens_setup\n")
#define CMD_SET_LENS_SETTINGS ("lens_setup %i %i %i %i %i %i %i %i %i %i %i\n")
#define CMD_SYNC_LENS_SETTINGS ("lens_setup ")
#define CMD_GET_LENS_SETTINGS_NO_PARAMS (11)

/**
 * @brief command "lens_active"
 *****************************************************************************/
#define CMD_GET_LENS_ACTIVE ("lens_active\n")
#define CMD_SET_LENS_ACTIVE ("lens_active %i\n")
#define CMD_SYNC_LENS_ACTIVE ("lens_active ")
#define CMD_GET_LENS_ACTIVE_NO_PARAMS (1)
#define CMD_SET_LENS_ACTIVE_TMO (125000)

/**
 * @brief command "lens_invert"
 *****************************************************************************/
#define CMD_GET_LENS_INVERT ("lens_invert\n")
#define CMD_SET_LENS_INVERT ("lens_invert %i %i %i %i\n")
#define CMD_SYNC_LENS_INVERT ("lens_invert ")
#define CMD_GET_LENS_INVERT_NO_PARAMS (4)

/**
 * @brief command "lens_auto_torque"
 *****************************************************************************/
#define CMD_GET_LENS_AUTO_TORQUE ("lens_auto_torque\n")
#define CMD_SET_LENS_AUTO_TORQUE ("lens_auto_torque %i\n")
#define CMD_SYNC_LENS_AUTO_TORQUE ("lens_auto_torque ")
#define CMD_GET_LENS_AUTO_TORQUE_NO_PARAMS (1)

/**
 * @brief command "focus_motor_pos"
 *****************************************************************************/
#define CMD_GET_LENS_FOCUS_MOTOR_POSITION ("focus_motor_pos\n")
#define CMD_SET_LENS_FOCUS_MOTOR_POSITION ("focus_motor_pos %i\n")
#define CMD_SYNC_LENS_FOCUS_MOTOR_POSITION ("focus_motor_pos ")
#define CMD_GET_LENS_FOCUS_MOTOR_POSITION_NO_PARAMS (1)

/**
 * @brief command "focus_pos"
 *****************************************************************************/
#define CMD_GET_LENS_FOCUS_POSITION ("focus_pos\n")
#define CMD_SET_LENS_FOCUS_POSITION ("focus_pos %i\n")
#define CMD_SYNC_LENS_FOCUS_POSITION ("focus_pos ")
#define CMD_GET_LENS_FOCUS_POSITION_NO_PARAMS (1)

/**
 * @brief command "focus_setup"
 *****************************************************************************/
#define CMD_GET_LENS_FOCUS_SETTINGS ("focus_setup\n")
#define CMD_SET_LENS_FOCUS_SETTINGS ("focus_setup %i %i %i \n")
#define CMD_SYNC_LENS_FOCUS_SETTINGS ("focus_setup ")
#define CMD_GET_LENS_FOCUS_SETTINGS_NO_PARAMS (3)

/**
 * @brief command "fine_focus"
 *****************************************************************************/
#define CMD_GET_LENS_FINE_FOCUS ("fine_focus\n")
#define CMD_SET_LENS_FINE_FOCUS ("fine_focus %i\n")
#define CMD_SYNC_LENS_FINE_FOCUS ("fine_focus ")
#define CMD_GET_LENS_FINE_FOCUS_NO_PARAMS (1)

/**
 * @brief command "zoom_pos"
 *****************************************************************************/
#define CMD_GET_LENS_ZOOM_POSITION ("zoom_pos\n")
#define CMD_SET_LENS_ZOOM_POSITION ("zoom_pos %i\n")
#define CMD_SYNC_LENS_ZOOM_POSITION ("zoom_pos ")
#define CMD_GET_LENS_ZOOM_POSITION_NO_PARAMS (1)

/**
 * @brief command "zoom_dir"
 *****************************************************************************/
#define CMD_GET_LENS_ZOOM_DIRECTION ("zoom_dir\n")
#define CMD_SET_LENS_ZOOM_DIRECTION ("zoom_dir %i\n")
#define CMD_SYNC_LENS_ZOOM_DIRECTION ("zoom_dir ")
#define CMD_GET_LENS_ZOOM_DIRECTION_NO_PARAMS (1)

/**
 * @brief command "zoom_setup"
 *****************************************************************************/
#define CMD_GET_LENS_ZOOM_SETTINGS ("zoom_setup\n")
#define CMD_SET_LENS_ZOOM_SETTINGS ("zoom_setup %i %i %i \n")
#define CMD_SYNC_LENS_ZOOM_SETTINGS ("zoom_setup ")
#define CMD_GET_LENS_ZOOM_SETTINGS_NO_PARAMS (3)

/**
 * @brief command "fine_zoom"
 *****************************************************************************/
#define CMD_GET_LENS_FINE_ZOOM ("fine_zoom\n")
#define CMD_SET_LENS_FINE_ZOOM ("fine_zoom %i\n")
#define CMD_SYNC_LENS_FINE_ZOOM ("fine_zoom ")
#define CMD_GET_LENS_FINE_ZOOM_NO_PARAMS (1)

/**
 * @brief command "iris_pos"
 *****************************************************************************/
#define CMD_GET_LENS_IRIS_POSITION ("iris_pos\n")
#define CMD_SET_LENS_IRIS_POSITION ("iris_pos %i\n")
#define CMD_SYNC_LENS_IRIS_POSITION ("iris_pos ")
#define CMD_GET_LENS_IRIS_POSITION_NO_PARAMS (1)

/**
 * @brief command "iris_setup"
 *****************************************************************************/
#define CMD_GET_LENS_IRIS_SETTINGS ("iris_setup\n")
#define CMD_SET_LENS_IRIS_SETTINGS ("iris_setup %i %i %i \n")
#define CMD_SYNC_LENS_IRIS_SETTINGS ("iris_setup ")
#define CMD_GET_LENS_IRIS_SETTINGS_NO_PARAMS (3)

/**
 * @brief command "iris_apt"
 *****************************************************************************/
#define CMD_GET_LENS_IRIS_APERTURE ("iris_apt\n")
#define CMD_SET_LENS_IRIS_APERTURE ("iris_apt %i\n")
#define CMD_SYNC_LENS_IRIS_APERTURE ("iris_apt ")
#define CMD_GET_LENS_IRIS_APERTURE_NO_PARAMS (1)

/**
 * @brief command "iris_table"
 *****************************************************************************/
#define CMD_GET_LENS_IRIS_TABLE ("iris_table\n")
#define CMD_SET_LENS_IRIS_TABLE                                                                    \
    ("iris_table %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i\n")
#define CMD_SYNC_LENS_IRIS_TABLE ("iris_table ")
#define CMD_GET_LENS_IRIS_TABLE_NO_PARAMS (18)
#define CMD_SET_LENS_IRIS_TABLE_TMO (30000)

/**
 * @brief command "fine_iris"
 *****************************************************************************/
#define CMD_GET_LENS_FINE_IRIS ("fine_iris\n")
#define CMD_SET_LENS_FINE_IRIS ("fine_iris %i\n")
#define CMD_SYNC_LENS_FINE_IRIS ("fine_iris ")
#define CMD_GET_LENS_FINE_IRIS_NO_PARAMS (1)

/**
 * @brief command "filter_pos"
 *****************************************************************************/
#define CMD_GET_LENS_FILTER_POSITION ("filter_pos\n")
#define CMD_SET_LENS_FILTER_POSITION ("filter_pos %i\n")
#define CMD_SYNC_LENS_FILTER_POSITION ("filter_pos ")
#define CMD_GET_LENS_FILTER_POSITION_NO_PARAMS (1)

/**
 * @brief command "filter_setup"
 *****************************************************************************/
#define CMD_GET_LENS_FILTER_SETTINGS ("filter_setup\n")
#define CMD_SET_LENS_FILTER_SETTINGS ("filter_setup %i %i %i \n")
#define CMD_SYNC_LENS_FILTER_SETTINGS ("filter_setup ")
#define CMD_GET_LENS_FILTER_SETTINGS_NO_PARAMS (3)

/**
 * @brief command "filter_pos"
 *****************************************************************************/
#define CMD_GET_LENS_MODE ("lens_mode\n")
#define CMD_SET_LENS_MODE ("lens_mode %i\n")
#define CMD_SYNC_LENS_MODE ("lens_mode ")
#define CMD_GET_LENS_MODE_NO_PARAMS (1)

/**
 * @brief command "motor_offset"
 *****************************************************************************/
#define CMD_GET_MOTOR_OFFSET ("motor_offset\n")
#define CMD_SET_MOTOR_OFFSET ("motor_offset %i\n")
#define CMD_SYNC_MOTOR_OFFSET ("motor_offset ")
#define CMD_GET_MOTOR_OFFSET_NO_PARAMS (1)

/**
 * @brief command "focus_offset"
 *****************************************************************************/
#define CMD_GET_FOCUS_OFFSET ("focus_offset\n")
#define CMD_SET_FOCUS_OFFSET ("focus_offset %i\n")
#define CMD_SYNC_FOCUS_OFFSET ("focus_offset ")
#define CMD_GET_FOCUS_OFFSET_NO_PARAMS (1)

/**
 * @brief command "lens_b4_info"
 *****************************************************************************/
#define CMD_GET_LENS_B4_INFO_1 ("lens_b4_info 1\n")
#define CMD_GET_LENS_B4_INFO_0 ("lens_b4_info 0\n")
#define CMD_SYNC_LENS_B4_INFO ("lens_b4_info ")
#define CMD_GET_LENS_B4_INFO_RESPONSE_1 ("lens_b4_info %80[^\t\r\n]\n")
#define CMD_GET_LENS_B4_INFO_NO_PARAMS_1 (1)
#define CMD_GET_LENS_B4_INFO_RESPONSE_0 ("lens_b4_info %02x %02x %02x %02x %02x\n")
#define CMD_GET_LENS_B4_INFO_NO_PARAMS_0 (5)

/**
 * @brief command "motor_rotating"
 *****************************************************************************/
#define CMD_GET_MOTOR_ROTATING ("motor_rotating\n")
#define CMD_SET_MOTOR_ROTATING ("motor_rotating %i\n")
#define CMD_SYNC_MOTOR_ROTATING ("motor_rotating ")
#define CMD_GET_MOTOR_ROTATING_NO_PARAMS (1)

/*
 * get_lens_settings
 *****************************************************************************/
static int get_lens_settings(void *ctx, ctrl_channel_handle_t channel, int num, int32_t *values)
{
    (void)ctx;

    // parameter check
    if (!values) {
        return (-EINVAL);
    }

    // return -EFAULT if number of parameter not matching
    if (num != CMD_GET_LENS_SETTINGS_NO_PARAMS) {
        return (-EFAULT);
    }

    // command call to get 11 parameters from provideo system
    int res = get_param_int_X(channel, 2, CMD_GET_LENS_SETTINGS, CMD_SYNC_LENS_SETTINGS,
                              CMD_SET_LENS_SETTINGS, &values[0], &values[1], &values[2], &values[3],
                              &values[4], &values[5], &values[6], &values[7], &values[8],
                              &values[9], &values[10]);

    // return error code
    if (res < 0) {
        return (res);
    }

    // return -EFAULT if number of parameter not matching
    if (res != CMD_GET_LENS_SETTINGS_NO_PARAMS) {
        return (-EFAULT);
    }

    return (0);
}

/*
 * set_lens_settings
 *****************************************************************************/
static int set_lens_settings(void *ctx, ctrl_channel_handle_t channel, int num, int32_t *values)
{
    (void)ctx;

    if (num != CMD_GET_LENS_SETTINGS_NO_PARAMS) {
        // return -EFAULT if number of parameter not matching
        return (-EFAULT);
    }

    return (set_param_int_X(channel, CMD_SET_LENS_SETTINGS, INT(values[0]), INT(values[1]),
                            INT(values[2]), INT(values[3]), INT(values[4]), INT(values[5]),
                            INT(values[6]), INT(values[7]), INT(values[8]), INT(values[9]),
                            INT(values[10])));
}

/*
 * get_lens_active
 *****************************************************************************/
static int get_lens_active(void *ctx, ctrl_channel_handle_t channel, int32_t *value)
{
    (void)ctx;

    // parameter check
    if (!value) {
        return (-EINVAL);
    }

    // command call to get 3 parameters from provideo system
    int res = get_param_int_X(channel, 2, CMD_GET_LENS_ACTIVE, CMD_SYNC_LENS_ACTIVE,
                              CMD_SET_LENS_ACTIVE, value);

    // return error code
    if (res < 0) {
        return (res);
    }

    // return -EFAULT if number of parameter not matching
    if (res != CMD_GET_LENS_ACTIVE_NO_PARAMS) {
        return (-EFAULT);
    }

    return (0);
}

/*
 * set_lens_settings
 *****************************************************************************/
static int set_lens_active(void *ctx, ctrl_channel_handle_t channel, int32_t value)
{
    (void)ctx;

    return (set_param_int_X_with_tmo(channel, CMD_SET_LENS_ACTIVE, CMD_SET_LENS_ACTIVE_TMO,
                                     INT(value)));
}

/*
 * get_lens_invert
 *****************************************************************************/
static int get_lens_invert(void *ctx, ctrl_channel_handle_t channel, int const num,
                           int32_t *const values)
{
    (void)ctx;

    // parameter check
    if (!values) {
        return (-EINVAL);
    }

    // return -EFAULT if number of parameter not matching
    if (num != CMD_GET_LENS_INVERT_NO_PARAMS) {
        return (-EFAULT);
    }

    // command call to get 4 parameters from provideo system
    int res = get_param_int_X(channel, 2, CMD_GET_LENS_INVERT, CMD_SYNC_LENS_INVERT,
                              CMD_SET_LENS_INVERT, &values[0], &values[1], &values[2], &values[3]);

    // return error code
    if (res < 0) {
        return (res);
    }

    // return -EFAULT if number of parameter not matching
    if (res != CMD_GET_LENS_INVERT_NO_PARAMS) {
        return (-EFAULT);
    }

    return (0);
}

/*
 * set_lens_settings
 *****************************************************************************/
static int set_lens_invert(void *ctx, ctrl_channel_handle_t channel, int num, int32_t *values)
{
    (void)ctx;

    if (num != CMD_GET_LENS_INVERT_NO_PARAMS) {
        // return -EFAULT if number of parameter not matching
        return (-EFAULT);
    }

    return (set_param_int_X(channel, CMD_SET_LENS_INVERT, INT(values[0]), INT(values[1]),
                            INT(values[2]), INT(values[3])));
}

/*
 * get_lens_auto_torque
 *****************************************************************************/
static int get_lens_auto_torque(void *ctx, ctrl_channel_handle_t channel, uint8_t *enable)
{
    (void)ctx;

    // parameter check
    if (enable == NULL) {
        return (-EINVAL);
    }

    // command call to get parameters from provideo system
    uint32_t tmp = 0;
    int res = get_param_int_X(channel, 2, CMD_GET_LENS_AUTO_TORQUE, CMD_SYNC_LENS_AUTO_TORQUE,
                              CMD_SET_LENS_AUTO_TORQUE, &tmp);
    if (tmp > UINT8_MAX) {
        return (-EILSEQ);
    }
    *enable = tmp;

    // return error code
    if (res < 0) {
        return (res);
    }

    // return -EFAULT if number of parameter not matching
    if (res != CMD_GET_LENS_AUTO_TORQUE_NO_PARAMS) {
        return (-EFAULT);
    }

    return (0);
}

/*
 * set_lens_auto_torque
 *****************************************************************************/
static int set_lens_auto_torque(void *ctx, ctrl_channel_handle_t channel, uint8_t enable)
{
    (void)ctx;

    // parameter check
    if (enable > 1) {
        return -EINVAL;
    }

    return (set_param_int_X(channel, CMD_SET_LENS_AUTO_TORQUE, INT(enable)));
}

/*
 * get_lens_focus_motor_position
 *****************************************************************************/
static int get_lens_focus_motor_position(void *ctx, ctrl_channel_handle_t channel, int32_t *value)
{
    (void)ctx;

    int res = 0;

    // parameter check
    if (value == NULL) {
        return (-EINVAL);
    }

    res = get_param_int_X(channel, 2, CMD_GET_LENS_FOCUS_MOTOR_POSITION,
                          CMD_SYNC_LENS_FOCUS_MOTOR_POSITION, CMD_SET_LENS_FOCUS_MOTOR_POSITION,
                          value);
    if (res < 0) {
        // use the focus_pos command if focus_motor_pos command is not supported
        res = get_param_int_X(channel, 2, CMD_GET_LENS_FOCUS_POSITION, CMD_SYNC_LENS_FOCUS_POSITION,
                              CMD_SET_LENS_FOCUS_POSITION, value);
        // return error code
        if (res < 0) {
            return (res);
        }
    }

    // return -EFAULT if number of parameter not matching
    if (res != CMD_GET_LENS_FOCUS_POSITION_NO_PARAMS) {
        return (-EFAULT);
    }

    return (0);
}

/*
 * set_lens_focus_motor_position
 *****************************************************************************/
static int set_lens_focus_motor_position(void *ctx, ctrl_channel_handle_t channel, int32_t value)
{
    (void)ctx;

    static bool has_focus_motor_pos_command = true;
    int res = 0;

    if (has_focus_motor_pos_command) {
        // TODO: parameter check
        res = set_param_int_X(channel, CMD_SET_LENS_FOCUS_MOTOR_POSITION, INT(value));

        if (res != 0) {
            // use focus_pos command from now on
            has_focus_motor_pos_command = false;
        }
    }

    if (!has_focus_motor_pos_command) {
        // parameter check
        if (value < 0 || value > DEFAULT_FINE_RANGE) {
            return -EINVAL;
        }
        res = set_param_int_X(channel, CMD_SET_LENS_FOCUS_POSITION, INT(value));
    }

    return res;
}

/*
 * get_lens_focus_position
 *****************************************************************************/
static int get_lens_focus_position(void *ctx, ctrl_channel_handle_t channel, int32_t *value)
{
    (void)ctx;

    // parameter check
    if (value == NULL) {
        return (-EINVAL);
    }

    // command call to get 3 parameters from provideo system
    int res = get_param_int_X(channel, 2, CMD_GET_LENS_FOCUS_POSITION, CMD_SYNC_LENS_FOCUS_POSITION,
                              CMD_SET_LENS_FOCUS_POSITION, value);

    // return error code
    if (res < 0) {
        return (res);
    }

    // return -EFAULT if number of parameter not matching
    if (res != CMD_GET_LENS_FOCUS_POSITION_NO_PARAMS) {
        return (-EFAULT);
    }

    return (0);
}

/*
 * set_lens_focus_position
 *****************************************************************************/
static int set_lens_focus_position(void *ctx, ctrl_channel_handle_t channel, int32_t value)
{
    (void)ctx;

    // parameter check
    if (value < 0 || value > DEFAULT_FINE_RANGE) {
        return -EINVAL;
    }

    return (set_param_int_X(channel, CMD_SET_LENS_FOCUS_POSITION, INT(value)));
}

/*
 * get_lens_focus_settings
 *****************************************************************************/
static int get_lens_focus_settings(void *ctx, ctrl_channel_handle_t channel, int num,
                                   int32_t *values)
{
    (void)ctx;

    // parameter check
    if (!values) {
        return (-EINVAL);
    }

    // return -EFAULT if number of parameter not matching
    if (num != CMD_GET_LENS_FOCUS_SETTINGS_NO_PARAMS) {
        return (-EFAULT);
    }

    // command call to get parameters from provideo system
    int res = get_param_int_X(channel, 2, CMD_GET_LENS_FOCUS_SETTINGS, CMD_SYNC_LENS_FOCUS_SETTINGS,
                              CMD_SET_LENS_FOCUS_SETTINGS, &values[0], &values[1], &values[2]);

    // return error code
    if (res < 0) {
        return (res);
    }

    // Special case: Some motor drives do not support the torque and/or step mode setting
    // and will therefore only send 1 or 2 values. In those cases set the other values to 0.
    if (res == 1) {
        values[1] = 0;
        values[2] = 0;
        return (res);
    }
    if (res == 2) {
        values[2] = 0;
        return (res);
    }

    // return -EFAULT if number of parameter not matching
    if (res != CMD_GET_LENS_FOCUS_SETTINGS_NO_PARAMS) {
        return (-EFAULT);
    }

    return (0);
}

/*
 * set_lens_focus_settings
 *****************************************************************************/
static int set_lens_focus_settings(void *ctx, ctrl_channel_handle_t channel, int num,
                                   int32_t *values)
{
    (void)ctx;

    if (num != CMD_GET_LENS_FOCUS_SETTINGS_NO_PARAMS) {
        // return -EFAULT if number of parameter not matching
        return (-EFAULT);
    }

    return (set_param_int_X(channel, CMD_SET_LENS_FOCUS_SETTINGS, INT(values[0]), INT(values[1]),
                            INT(values[2])));
}

/*
 * get_lens_fine_focus
 *****************************************************************************/
static int get_lens_fine_focus(void *ctx, ctrl_channel_handle_t channel, uint8_t *value)
{
    (void)ctx;

    // parameter check
    if (value == NULL) {
        return (-EINVAL);
    }
    // command call to get 3 parameters from provideo system
    uint32_t focus = 0;
    int res = get_param_int_X(channel, 2, CMD_GET_LENS_FINE_FOCUS, CMD_SYNC_LENS_FINE_FOCUS,
                              CMD_SET_LENS_FINE_FOCUS, &focus);

    if (focus > UINT8_MAX) {
        return (-EILSEQ);
    }
    *value = focus;

    // return error code
    if (res < 0) {
        return (res);
    }

    // return -EFAULT if number of parameter not matching
    if (res != CMD_GET_LENS_FINE_FOCUS_NO_PARAMS) {
        return (-EFAULT);
    }

    return (0);
}

/*
 * set_lens_fine_focus
 *****************************************************************************/
static int set_lens_fine_focus(void *ctx, ctrl_channel_handle_t channel, uint8_t value)
{
    (void)ctx;

    // parameter check
    if (value > 1) {
        return -EINVAL;
    }

    return (set_param_int_X(channel, CMD_SET_LENS_FINE_FOCUS, INT(value)));
}

/*
 * get_lens_zoom_position
 *****************************************************************************/
static int get_lens_zoom_position(void *ctx, ctrl_channel_handle_t channel, int32_t *value)
{
    (void)ctx;

    // parameter check
    if (value == NULL) {
        return (-EINVAL);
    }

    // command call to get 3 parameters from provideo system
    int res = get_param_int_X(channel, 2, CMD_GET_LENS_ZOOM_POSITION, CMD_SYNC_LENS_ZOOM_POSITION,
                              CMD_SET_LENS_ZOOM_POSITION, value);

    // return error code
    if (res < 0) {
        return (res);
    }

    // return -EFAULT if number of parameter not matching
    if (res != CMD_GET_LENS_ZOOM_POSITION_NO_PARAMS) {
        return (-EFAULT);
    }

    return (0);
}

/*
 * set_lens_zoom_position
 *****************************************************************************/
static int set_lens_zoom_position(void *ctx, ctrl_channel_handle_t channel, int32_t value)
{
    (void)ctx;

    // parameter check
    if (value < 0 || value > DEFAULT_FINE_RANGE) {
        return -EINVAL;
    }

    return (set_param_int_X(channel, CMD_SET_LENS_ZOOM_POSITION, INT(value)));
}

/*
 * get_lens_zoom_direction
 *****************************************************************************/
static int get_lens_zoom_direction(void *ctx, ctrl_channel_handle_t channel, int32_t *value)
{
    (void)ctx;

    // parameter check
    if (value == NULL) {
        return (-EINVAL);
    }

    // command call to get 1 parameter from provideo system
    int ret = get_param_int_X(channel, 2, CMD_GET_LENS_ZOOM_DIRECTION, CMD_SYNC_LENS_ZOOM_DIRECTION,
                              CMD_SET_LENS_ZOOM_DIRECTION, value);

    // return error code
    if (ret < 0) {
        return (ret);
    }

    // return -EFAULT if number of parameter not matching
    if (ret != CMD_GET_LENS_ZOOM_DIRECTION_NO_PARAMS) {
        return (-EFAULT);
    }

    return (0);
}

/*
 * set_lens_zoom_direction
 *****************************************************************************/
static int set_lens_zoom_direction(void *ctx, ctrl_channel_handle_t channel, int32_t value)
{
    (void)ctx;

    // parameter check
    if (value < -DEFAULT_RANGE || value > DEFAULT_RANGE) {
        return -EINVAL;
    }

    return (set_param_int_X(channel, CMD_SET_LENS_ZOOM_DIRECTION, INT(value)));
}

/*
 * get_lens_zoom_settings
 *****************************************************************************/
static int get_lens_zoom_settings(void *ctx, ctrl_channel_handle_t channel, int num,
                                  int32_t *values)
{
    (void)ctx;

    // parameter check
    if (!values) {
        return (-EINVAL);
    }

    // return -EFAULT if number of parameter not matching
    if (num != CMD_GET_LENS_ZOOM_SETTINGS_NO_PARAMS) {
        return (-EFAULT);
    }

    // command call to get parameters from provideo system
    int res = get_param_int_X(channel, 2, CMD_GET_LENS_ZOOM_SETTINGS, CMD_SYNC_LENS_ZOOM_SETTINGS,
                              CMD_SET_LENS_ZOOM_SETTINGS, &values[0], &values[1], &values[2]);

    // return error code
    if (res < 0) {
        return (res);
    }

    // Special case: Some motor drives do not support the torque and/or step mode setting
    // and will therefore only send 1 or 2 values. In those cases set the other values to 0.
    if (res == 1) {
        values[1] = 0;
        values[2] = 0;
        return (res);
    }
    if (res == 2) {
        values[2] = 0;
        return (res);
    }

    // return -EFAULT if number of parameter not matching
    if (res != CMD_GET_LENS_ZOOM_SETTINGS_NO_PARAMS) {
        return (-EFAULT);
    }

    return (0);
}

/*
 * set_lens_zoom_settings
 *****************************************************************************/
static int set_lens_zoom_settings(void *ctx, ctrl_channel_handle_t channel, int num,
                                  int32_t *values)
{
    (void)ctx;

    if (num != CMD_GET_LENS_ZOOM_SETTINGS_NO_PARAMS) {
        // return -EFAULT if number of parameter not matching
        return (-EFAULT);
    }

    return (set_param_int_X(channel, CMD_SET_LENS_ZOOM_SETTINGS, INT(values[0]), INT(values[1]),
                            INT(values[2])));
}

/*
 * get_lens_fine_zoom
 *****************************************************************************/
static int get_lens_fine_zoom(void *ctx, ctrl_channel_handle_t channel, uint8_t *value)
{
    (void)ctx;

    // parameter check
    if (value == NULL) {
        return (-EINVAL);
    }

    // command call to get 3 parameters from provideo system
    uint32_t tmp = 0;
    int res = get_param_int_X(channel, 2, CMD_GET_LENS_FINE_ZOOM, CMD_SYNC_LENS_FINE_ZOOM,
                              CMD_SET_LENS_FINE_ZOOM, &tmp);
    if (tmp > UINT8_MAX) {
        return (-EILSEQ);
    }
    *value = tmp;

    // return error code
    if (res < 0) {
        return (res);
    }

    // return -EFAULT if number of parameter not matching
    if (res != CMD_GET_LENS_FINE_ZOOM_NO_PARAMS) {
        return (-EFAULT);
    }

    return (0);
}

/*
 * set_lens_fine_zoom
 *****************************************************************************/
static int set_lens_fine_zoom(void *ctx, ctrl_channel_handle_t channel, uint8_t value)
{
    (void)ctx;

    // parameter check
    if (value > 1) {
        return -EINVAL;
    }

    return (set_param_int_X(channel, CMD_SET_LENS_FINE_ZOOM, INT(value)));
}

/*
 * get_lens_iris_position
 *****************************************************************************/
static int get_lens_iris_position(void *ctx, ctrl_channel_handle_t channel, int32_t *value)
{
    (void)ctx;

    // parameter check
    if (value == NULL) {
        return (-EINVAL);
    }

    // command call to get 3 parameters from provideo system
    int res = get_param_int_X(channel, 2, CMD_GET_LENS_IRIS_POSITION, CMD_SYNC_LENS_IRIS_POSITION,
                              CMD_SET_LENS_IRIS_POSITION, value);

    // return error code
    if (res < 0) {
        return (res);
    }

    // return -EFAULT if number of parameter not matching
    if (res != CMD_GET_LENS_IRIS_POSITION_NO_PARAMS) {
        return (-EFAULT);
    }

    return (0);
}

/*
 * set_lens_iris_position
 *****************************************************************************/
static int set_lens_iris_position(void *ctx, ctrl_channel_handle_t channel, int32_t value)
{
    (void)ctx;

    // parameter check
    if (value < 0 || value > DEFAULT_FINE_RANGE) {
        return -EINVAL;
    }

    return (set_param_int_X(channel, CMD_SET_LENS_IRIS_POSITION, INT(value)));
}

/*
 * get_lens_iris_settings
 *****************************************************************************/
static int get_lens_iris_settings(void *ctx, ctrl_channel_handle_t channel, int num,
                                  int32_t *values)
{
    (void)ctx;

    // parameter check
    if (!values) {
        return (-EINVAL);
    }

    // return -EFAULT if number of parameter not matching
    if (num != CMD_GET_LENS_IRIS_SETTINGS_NO_PARAMS) {
        return (-EFAULT);
    }

    // command call to get parameters from provideo system
    int res = get_param_int_X(channel, 2, CMD_GET_LENS_IRIS_SETTINGS, CMD_SYNC_LENS_IRIS_SETTINGS,
                              CMD_SET_LENS_IRIS_SETTINGS, &values[0], &values[1], &values[2]);

    // return error code
    if (res < 0) {
        return (res);
    }

    // Special case: Some motor drives do not support the torque and/or step mode setting
    // and will therefore only send 1 or 2 values. In those cases set the other values to 0.
    if (res == 1) {
        values[1] = 0;
        values[2] = 0;
        return (res);
    }
    if (res == 2) {
        values[2] = 0;
        return (res);
    }

    // return -EFAULT if number of parameter not matching
    if (res != CMD_GET_LENS_IRIS_SETTINGS_NO_PARAMS) {
        return (-EFAULT);
    }

    return (0);
}

/*
 * set_lens_iris_settings
 *****************************************************************************/
static int set_lens_iris_settings(void *ctx, ctrl_channel_handle_t channel, int num,
                                  int32_t *values)
{
    (void)ctx;

    if (num != CMD_GET_LENS_IRIS_SETTINGS_NO_PARAMS) {
        // return -EFAULT if number of parameter not matching
        return (-EFAULT);
    }

    return (set_param_int_X(channel, CMD_SET_LENS_IRIS_SETTINGS, INT(values[0]), INT(values[1]),
                            INT(values[2])));
}

/*
 * get_lens_iris_aperture
 *****************************************************************************/
static int get_lens_iris_aperture(void *ctx, ctrl_channel_handle_t channel, int32_t *value)
{
    (void)ctx;

    // parameter check
    if (value == NULL) {
        return (-EINVAL);
    }

    // command call to get 1 parameters from provideo system
    int res = get_param_int_X(channel, 2, CMD_GET_LENS_IRIS_APERTURE, CMD_SYNC_LENS_IRIS_APERTURE,
                              CMD_SET_LENS_IRIS_APERTURE, value);

    // return error code
    if (res < 0) {
        return (res);
    }

    // return -EFAULT if number of parameter not matching
    if (res != CMD_GET_LENS_IRIS_APERTURE_NO_PARAMS) {
        return (-EFAULT);
    }

    return (0);
}

/*
 * set_lens_iris_aperture
 *****************************************************************************/
static int set_lens_iris_aperture(void *ctx, ctrl_channel_handle_t channel, int32_t value)
{
    (void)ctx;

    // parameter check
    if (value < 0 || value > DEFAULT_FINE_RANGE) {
        return -EINVAL;
    }
    return (set_param_int_X(channel, CMD_SET_LENS_IRIS_APERTURE, INT(value)));
}

/*
 * get_lens_iris_table
 *****************************************************************************/
static int get_lens_iris_table(void *ctx, ctrl_channel_handle_t channel, int num, int32_t *values)
{
    (void)ctx;

    // parameter check
    if (!values) {
        return (-EINVAL);
    }

    // return -EFAULT if number of parameter not matching
    if (num != CMD_GET_LENS_IRIS_TABLE_NO_PARAMS) {
        return (-EFAULT);
    }

    // command call to get 11 parameters from provideo system
    int res = get_param_int_X(channel, 2, CMD_GET_LENS_IRIS_TABLE, CMD_SYNC_LENS_IRIS_TABLE,
                              CMD_SET_LENS_IRIS_TABLE, &values[0], &values[1], &values[2],
                              &values[3], &values[4], &values[5], &values[6], &values[7],
                              &values[8], &values[9], &values[10], &values[11], &values[12],
                              &values[13], &values[14], &values[15], &values[16], &values[17]);

    // return error code
    if (res < 0) {
        return (res);
    }

    // return -EFAULT if number of parameter not matching
    if (res != CMD_GET_LENS_IRIS_TABLE_NO_PARAMS) {
        return (-EFAULT);
    }

    return (0);
}

/*
 * set_lens_iris_table
 *****************************************************************************/
static int set_lens_iris_table(void *ctx, ctrl_channel_handle_t channel, int num,
                               int32_t *const values)
{
    (void)ctx;

    if (num != CMD_GET_LENS_IRIS_TABLE_NO_PARAMS) {
        // return -EFAULT if number of parameter not matching
        return (-EFAULT);
    }

    return (set_param_int_X_with_tmo(
            channel, CMD_SET_LENS_IRIS_TABLE, CMD_SET_LENS_IRIS_TABLE_TMO, INT(values[0]),
            INT(values[1]), INT(values[2]), INT(values[3]), INT(values[4]), INT(values[5]),
            INT(values[6]), INT(values[7]), INT(values[8]), INT(values[9]), INT(values[10]),
            INT(values[11]), INT(values[12]), INT(values[13]), INT(values[14]), INT(values[15]),
            INT(values[16]), INT(values[17])));
}

/*
 * get_lens_fine_focus
 *****************************************************************************/
static int get_lens_fine_iris(void *ctx, ctrl_channel_handle_t channel, uint8_t *value)
{
    (void)ctx;

    // parameter check
    if (value == NULL) {
        return (-EINVAL);
    }
    // command call to get 1 parameters from provideo system
    uint32_t fine_iris = 0;
    int res = get_param_int_X(channel, 2, CMD_GET_LENS_FINE_IRIS, CMD_SYNC_LENS_FINE_IRIS,
                              CMD_SET_LENS_FINE_IRIS, &fine_iris);

    if (fine_iris > UINT8_MAX) {
        return (-EILSEQ);
    }
    *value = fine_iris;

    // return error code
    if (res < 0) {
        return (res);
    }

    // return -EFAULT if number of parameter not matching
    if (res != CMD_GET_LENS_FINE_IRIS_NO_PARAMS) {
        return (-EFAULT);
    }

    return (0);
}

/*
 * set_lens_fine_iris
 *****************************************************************************/
static int set_lens_fine_iris(void *ctx, ctrl_channel_handle_t channel, uint8_t value)
{
    (void)ctx;

    // parameter check
    if (value > 1) {
        return -EINVAL;
    }

    return (set_param_int_X(channel, CMD_SET_LENS_FINE_IRIS, INT(value)));
}

/*
 * get_lens_filter_position
 *****************************************************************************/
static int get_lens_filter_position(void *ctx, ctrl_channel_handle_t channel, int32_t *value)
{
    (void)ctx;

    // parameter check
    if (value == NULL) {
        return (-EINVAL);
    }

    // command call to get 3 parameters from provideo system
    int res = get_param_int_X(channel, 2, CMD_GET_LENS_FILTER_POSITION,
                              CMD_SYNC_LENS_FILTER_POSITION, CMD_SET_LENS_FILTER_POSITION, value);

    // return error code
    if (res < 0) {
        return (res);
    }

    // return -EFAULT if number of parameter not matching
    if (res != CMD_GET_LENS_FILTER_POSITION_NO_PARAMS) {
        return (-EFAULT);
    }

    return (0);
}

/*
 * set_lens_filter_position
 *****************************************************************************/
static int set_lens_filter_position(void *ctx, ctrl_channel_handle_t channel, int32_t value)
{
    (void)ctx;

    // parameter check
    if (value < 0 || value > DEFAULT_RANGE) {
        return -EINVAL;
    }

    return (set_param_int_X(channel, CMD_SET_LENS_FILTER_POSITION, INT(value)));
}

/*
 * get_lens_filter_settings
 *****************************************************************************/
static int get_lens_filter_settings(void *ctx, ctrl_channel_handle_t channel, int num,
                                    int32_t *values)
{
    (void)ctx;

    // parameter check
    if (!values) {
        return (-EINVAL);
    }

    // return -EFAULT if number of parameter not matching
    if (num != CMD_GET_LENS_FILTER_SETTINGS_NO_PARAMS) {
        return (-EFAULT);
    }

    // command call to get parameters from provideo system
    int res =
            get_param_int_X(channel, 2, CMD_GET_LENS_FILTER_SETTINGS, CMD_SYNC_LENS_FILTER_SETTINGS,
                            CMD_SET_LENS_FILTER_SETTINGS, &values[0], &values[1], &values[2]);

    // return error code
    if (res < 0) {
        return (res);
    }

    // Special case: Some motor drives do not support the torque and/or step mode setting
    // and will therefore only send 1 or 2 values. In those cases set the other values to 0.
    if (res == 1) {
        values[1] = 0;
        values[2] = 0;
        return (res);
    }
    if (res == 2) {
        values[2] = 0;
        return (res);
    }

    // return -EFAULT if number of parameter not matching
    if (res != CMD_GET_LENS_FILTER_SETTINGS_NO_PARAMS) {
        return (-EFAULT);
    }

    return (0);
}

/*
 * set_lens_filter_settings
 *****************************************************************************/
static int set_lens_filter_settings(void *ctx, ctrl_channel_handle_t channel, int num,
                                    int32_t *values)
{
    (void)ctx;

    if (num != CMD_GET_LENS_FILTER_SETTINGS_NO_PARAMS) {
        // return -EFAULT if number of parameter not matching
        return (-EFAULT);
    }

    return (set_param_int_X(channel, CMD_SET_LENS_FILTER_SETTINGS, INT(values[0]), INT(values[1]),
                            INT(values[2])));
}

/*
 * get_lens_mode
 *****************************************************************************/
static int get_lens_mode(void *ctx, ctrl_channel_handle_t channel, uint16_t *mode)
{
    (void)ctx;

    // parameter check
    if (mode == NULL) {
        return (-EINVAL);
    }

    // command call to get parameters from provideo system
    int value = 0;
    int res = get_param_int_X(channel, 2, CMD_GET_LENS_MODE, CMD_SYNC_LENS_MODE, CMD_SET_LENS_MODE,
                              &value);

    // return error code
    if (res < 0) {
        return (res);
    }

    // return -EFAULT if number of parameter not matching
    if (res != CMD_GET_LENS_MODE_NO_PARAMS) {
        return (-EFAULT);
    }

    // type-cast to range
    *mode = UINT16(value);

    return (0);
}

/*
 * set_lens_mode
 *****************************************************************************/
static int set_lens_mode(void *ctx, ctrl_channel_handle_t channel, uint16_t mode)
{
    (void)ctx;

    return (set_param_int_X(channel, CMD_SET_LENS_MODE, INT(mode)));
}

/*
 * get_motor_offset
 *****************************************************************************/
static int get_motor_offset(void *ctx, ctrl_channel_handle_t channel, uint16_t *offset)
{
    (void)ctx;

    // parameter check
    if (offset == NULL) {
        return (-EINVAL);
    }

    // command call to get parameters from provideo system
    int value = 0;
    int res = get_param_int_X(channel, 2, CMD_GET_MOTOR_OFFSET, CMD_SYNC_MOTOR_OFFSET,
                              CMD_SET_MOTOR_OFFSET, &value);

    // return error code
    if (res < 0) {
        return (res);
    }

    // return -EFAULT if number of parameter not matching
    if (res != CMD_GET_MOTOR_OFFSET_NO_PARAMS) {
        return (-EFAULT);
    }

    // type-cast to range
    *offset = UINT16(value);

    return (0);
}

/*
 * set_motor_offset
 *****************************************************************************/
static int set_motor_offset(void *ctx, ctrl_channel_handle_t channel, uint16_t offset)
{
    (void)ctx;

    return (set_param_int_X(channel, CMD_SET_MOTOR_OFFSET, INT(offset)));
}

/*
 * get_focus_offset
 *****************************************************************************/
static int get_focus_offset(void *ctx, ctrl_channel_handle_t channel, uint16_t *offset)
{
    (void)ctx;

    // parameter check
    if (offset == NULL) {
        return (-EINVAL);
    }

    // command call to get parameters from provideo system
    int value = 0;
    int res = get_param_int_X(channel, 2, CMD_GET_FOCUS_OFFSET, CMD_SYNC_FOCUS_OFFSET,
                              CMD_SET_FOCUS_OFFSET, &value);

    // return error code
    if (res < 0) {
        return (res);
    }

    // return -EFAULT if number of parameter not matching
    if (res != CMD_GET_FOCUS_OFFSET_NO_PARAMS) {
        return (-EFAULT);
    }

    // type-cast to range
    *offset = UINT16(value);

    return (0);
}

/*
 * set_focus_offset
 *****************************************************************************/
static int set_focus_offset(void *ctx, ctrl_channel_handle_t channel, uint16_t offset)
{
    (void)ctx;

    return (set_param_int_X(channel, CMD_SET_FOCUS_OFFSET, INT(offset)));
}

/*
 * get_lens_b4_info
 *****************************************************************************/
static int get_lens_b4_info(void *ctx, ctrl_channel_handle_t channel, int const no, uint8_t *info)
{
    (void)ctx;

    // parameter check
    if (info == NULL) {
        return (-EINVAL);
    }

    if (no == 1) {
        // command call to get a string from provideo system
        int res = get_param_string(channel, 1, CMD_GET_LENS_B4_INFO_1, CMD_SYNC_LENS_B4_INFO,
                                   CMD_GET_LENS_B4_INFO_RESPONSE_1, (char *)info);

        // return error code
        if (res < 0) {
            return (res);
        }

        // number of parameter is not matching
        if (res != CMD_GET_LENS_B4_INFO_NO_PARAMS_1) {
            return (-EFAULT);
        }
    } else if (no == 0) {
        int value0 = 0;
        int value1 = 0;
        int value2 = 0;
        int value3 = 0;
        int value4 = 0;
        // command call to get an uint8_t array from provideo system
        int res = get_param_int_X(channel, 1, CMD_GET_LENS_B4_INFO_0, CMD_SYNC_LENS_B4_INFO,
                                  CMD_GET_LENS_B4_INFO_RESPONSE_0, value0, value1, value2, value3,
                                  value4);

        if (res < 0) {
            return res;
        }

        if (res != CMD_GET_LENS_B4_INFO_NO_PARAMS_0) {
            return (-EFAULT);
        }
        info[0] = value0;
        info[1] = value1;
        info[2] = value2;
        info[3] = value3;
        info[4] = value4;

    } else {
        return (-EINVAL);
    }

    return 0;
}

/*
 * get_motor_rotating
 *****************************************************************************/
static int get_motor_rotating(void *ctx, ctrl_channel_handle_t channel, uint8_t *motor_rotating)
{
    (void)ctx;

    // parameter check
    if (motor_rotating == NULL) {
        return (-EINVAL);
    }

    // command call to get parameters from provideo system
    int value = 0;
    int res = get_param_int_X(channel, 2, CMD_GET_MOTOR_ROTATING, CMD_SYNC_MOTOR_ROTATING,
                              CMD_SET_MOTOR_ROTATING, &value);

    if (res < 0) { // return error code
        return res;
    }
    if (res != CMD_GET_MOTOR_ROTATING_NO_PARAMS) {
        // parameter mismatch => -EFAULT
        return (-EFAULT);
    }

    // type-cast to range
    *motor_rotating = UINT8(value);

    return 0;
}

/*
 * Lens protocol driver declaration
 *****************************************************************************/
static const ctrl_protocol_lens_drv_t provideo_lens_drv = {
    .get_lens_settings = get_lens_settings,
    .set_lens_settings = set_lens_settings,
    .get_lens_active = get_lens_active,
    .set_lens_active = set_lens_active,

    .get_lens_invert = get_lens_invert,
    .set_lens_invert = set_lens_invert,
    .get_lens_auto_torque = get_lens_auto_torque,
    .set_lens_auto_torque = set_lens_auto_torque,

    .get_lens_focus_motor_position = get_lens_focus_motor_position,
    .set_lens_focus_motor_position = set_lens_focus_motor_position,
    .get_lens_focus_position = get_lens_focus_position,
    .set_lens_focus_position = set_lens_focus_position,
    .get_lens_focus_settings = get_lens_focus_settings,
    .set_lens_focus_settings = set_lens_focus_settings,
    .get_lens_fine_focus = get_lens_fine_focus,
    .set_lens_fine_focus = set_lens_fine_focus,

    .get_lens_zoom_position = get_lens_zoom_position,
    .set_lens_zoom_position = set_lens_zoom_position,
    .get_lens_zoom_direction = get_lens_zoom_direction,
    .set_lens_zoom_direction = set_lens_zoom_direction,
    .get_lens_zoom_settings = get_lens_zoom_settings,
    .set_lens_zoom_settings = set_lens_zoom_settings,
    .get_lens_fine_zoom = get_lens_fine_zoom,
    .set_lens_fine_zoom = set_lens_fine_zoom,

    .get_lens_iris_position = get_lens_iris_position,
    .set_lens_iris_position = set_lens_iris_position,
    .get_lens_iris_settings = get_lens_iris_settings,
    .set_lens_iris_settings = set_lens_iris_settings,
    .get_lens_iris_aperture = get_lens_iris_aperture,
    .set_lens_iris_aperture = set_lens_iris_aperture,
    .get_lens_iris_table = get_lens_iris_table,
    .set_lens_iris_table = set_lens_iris_table,
    .get_lens_fine_iris = get_lens_fine_iris,
    .set_lens_fine_iris = set_lens_fine_iris,

    .get_lens_filter_position = get_lens_filter_position,
    .set_lens_filter_position = set_lens_filter_position,
    .get_lens_filter_settings = get_lens_filter_settings,
    .set_lens_filter_settings = set_lens_filter_settings,

    .get_lens_mode = get_lens_mode,
    .set_lens_mode = set_lens_mode,
    .get_motor_offset = get_motor_offset,
    .set_motor_offset = set_motor_offset,
    .get_focus_offset = get_focus_offset,
    .set_focus_offset = set_focus_offset,

    .get_lens_b4_info = get_lens_b4_info,

    .get_motor_rotating = get_motor_rotating
};

/*
 * provideo_protocol_lens_init
 *****************************************************************************/
int provideo_protocol_lens_init(ctrl_protocol_handle_t handle, void *ctx)
{
    return (ctrl_protocol_lens_register(handle, ctx, &provideo_lens_drv));
}
