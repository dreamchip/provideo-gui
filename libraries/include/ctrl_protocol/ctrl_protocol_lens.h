/*
 * Copyright (C) 2017 Dream Chip Technologies GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
/**
 * @file    ctrl_protocol_lens.h
 *
 * @brief   Lens control functions
 *
 *****************************************************************************/
#ifndef CTRL_PROTOCOL_LENS_H
#define CTRL_PROTOCOL_LENS_H

#include <stdint.h>

#include <ctrl_channel/ctrl_channel.h>
#include <ctrl_protocol/ctrl_protocol.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @defgroup ctrl_protocol_layer Generic Control Protocol Layer implementation
 * @{
 *****************************************************************************/

/**
 * @brief Number of lens setting values
 *****************************************************************************/
#define NO_VALUES_LENS_SETTINGS (11)
#define NO_VALUES_LENS_INVERT (4)
#define NO_VALUES_LENS_FOCUS_SETTINGS (3)
#define NO_VALUES_LENS_ZOOM_SETTINGS (3)
#define NO_VALUES_LENS_IRIS_SETTINGS (3)
#define NO_VALUES_LENS_IRIS_TABLE (18)
#define NO_VALUES_LENS_FILTER_SETTINGS (3)

/**
 * @brief Gets the current lens settings
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[in]   no       number of array/list items
 * @param[out]  values   array (memory pointer) to fill
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_get_lens_settings(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                    int num, int32_t *values);

/**
 * @brief Sets lens settings
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[in]   num      number of array/list items
 * @param[out]  values   array (memory pointer) to fill
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_set_lens_settings(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                    int num, int32_t *values);

/**
 * @brief Gets the current active status
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[out]  act      pointer to active value
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_get_lens_active(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                  int32_t *act);

/**
 * @brief Sets active status
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[out]  act      new active status
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_set_lens_active(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                  int32_t act);

/**
 * @brief Gets the current invert status
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[in]   num      number of array/list items
 * @param[out]  values   array (memory pointer) to fill
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_get_lens_invert(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                  int num, int32_t *values);

/**
 * @brief Sets invert status
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[in]   num      number of array/list items
 * @param[out]  values   array (memory pointer) to fillus
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_set_lens_invert(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                  int num, int32_t *values);

/**
 * @brief Gets automatic torque calibration enable
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[out]  enable   pointer to returned enable flag
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_get_lens_auto_torque(ctrl_protocol_handle_t protocol,
                                       ctrl_channel_handle_t channel, uint8_t *enable);

/**
 * @brief Enables or disables automatic torque calibration
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[out]  enable   enable flag
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_set_lens_auto_torque(ctrl_protocol_handle_t protocol,
                                       ctrl_channel_handle_t channel, uint8_t enable);

/**
 * @brief Gets the focus motor position
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[out]  pos      pointer to position value
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_get_lens_focus_motor_position(ctrl_protocol_handle_t protocol,
                                                ctrl_channel_handle_t channel, int32_t *pos);

/**
 * @brief Sets focus motor position value
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[out]  pos      pointer to position value
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_set_lens_focus_motor_position(ctrl_protocol_handle_t protocol,
                                                ctrl_channel_handle_t channel, int32_t pos);

/**
 * @brief Gets the focus position
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[out]  pos      pointer to position value
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_get_lens_focus_position(ctrl_protocol_handle_t protocol,
                                          ctrl_channel_handle_t channel, int32_t *pos);

/**
 * @brief Sets focus position value
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[out]  pos      pointer to position value
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_set_lens_focus_position(ctrl_protocol_handle_t protocol,
                                          ctrl_channel_handle_t channel, int32_t pos);

/**
 * @brief Gets the current lens focus settings
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[in]   num      number of array/list items
 * @param[out]  values   array (memory pointer) to fill
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_get_lens_focus_settings(ctrl_protocol_handle_t protocol,
                                          ctrl_channel_handle_t channel, int num, int32_t *values);

/**
 * @brief Sets lens focus settings
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[in]   num      number of array/list items
 * @param[out]  values   array (memory pointer) to fill
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_set_lens_focus_settings(ctrl_protocol_handle_t protocol,
                                          ctrl_channel_handle_t channel, int num, int32_t *values);

/**
 * @brief Gets the fine focus enable status
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[out]  enable   pointer to returned enable flag
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_get_lens_fine_focus(ctrl_protocol_handle_t protocol,
                                      ctrl_channel_handle_t channel, uint8_t *enable);

/**
 * @brief Enable or disable fine focus
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[out]  enable   enable flag
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_set_lens_fine_focus(ctrl_protocol_handle_t protocol,
                                      ctrl_channel_handle_t channel, uint8_t enable);

/**
 * @brief Gets the zoom position
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[out]  pos      pointer to position value
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_get_lens_zoom_position(ctrl_protocol_handle_t protocol,
                                         ctrl_channel_handle_t channel, int32_t *pos);

/**
 * @brief Sets zoom position value
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[out]  pos      pointer to position value
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_set_lens_zoom_position(ctrl_protocol_handle_t protocol,
                                         ctrl_channel_handle_t channel, int32_t pos);

/**
 * @brief Gets the zoom direction
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[out]  dir      pointer to position value
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_get_lens_zoom_direction(ctrl_protocol_handle_t protocol,
                                          ctrl_channel_handle_t channel, int32_t *dir);

/**
 * @brief Sets zoom direction value
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[out]  dir      pointer to position value
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_set_lens_zoom_direction(ctrl_protocol_handle_t protocol,
                                          ctrl_channel_handle_t channel, int32_t dir);

/**
 * @brief Gets the current lens zoom settings
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[in]   num      number of array/list items
 * @param[out]  values   array (memory pointer) to fill
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_get_lens_zoom_settings(ctrl_protocol_handle_t protocol,
                                         ctrl_channel_handle_t channel, int num, int32_t *values);

/**
 * @brief Sets lens zoom settings
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[in]   num      number of array/list items
 * @param[out]  values   array (memory pointer) to fill
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_set_lens_zoom_settings(ctrl_protocol_handle_t protocol,
                                         ctrl_channel_handle_t channel, int num, int32_t *values);

/**
 * @brief Gets the fine zoom enable status
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[out]  enable   pointer to returned enable flag
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_get_lens_fine_zoom(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                     uint8_t *enable);

/**
 * @brief Enable or disable fine zoom
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[out]  enable   enable flag
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_set_lens_fine_zoom(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                     uint8_t enable);

/**
 * @brief Gets the iris position
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[out]  pos      pointer to position value
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_get_lens_iris_position(ctrl_protocol_handle_t protocol,
                                         ctrl_channel_handle_t channel, int32_t *act);

/**
 * @brief Sets iris position value
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[out]  pos      pointer to position value
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_set_lens_iris_position(ctrl_protocol_handle_t protocol,
                                         ctrl_channel_handle_t channel, int32_t pos);

/**
 * @brief Gets the current lens iris settings
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[in]   num      number of array/list items
 * @param[out]  values   array (memory pointer) to fill
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_get_lens_iris_settings(ctrl_protocol_handle_t protocol,
                                         ctrl_channel_handle_t channel, int num, int32_t *values);

/**
 * @brief Sets lens iris settings
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[in]   num      number of array/list items
 * @param[out]  values   array (memory pointer) to fill
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_set_lens_iris_settings(ctrl_protocol_handle_t protocol,
                                         ctrl_channel_handle_t channel, int num, int32_t *values);

/**
 * @brief Gets the iris aperture
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[out]  apt      pointer to position value
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_get_lens_iris_aperture(ctrl_protocol_handle_t protocol,
                                         ctrl_channel_handle_t channel, int32_t *apt);

/**
 * @brief Sets iris aperture value
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[out]  apt      pointer to position value
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_set_lens_iris_aperture(ctrl_protocol_handle_t protocol,
                                         ctrl_channel_handle_t channel, int32_t apt);

/**
 * @brief Gets the current lens iris table
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[in]   num      number of array/list items
 * @param[out]  values   array (memory pointer) to fill
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_get_lens_iris_table(ctrl_protocol_handle_t protocol,
                                      ctrl_channel_handle_t channel, int num, int32_t *values);

/**
 * @brief Sets lens iris table
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[in]   num      number of array/list items
 * @param[out]  values   array (memory pointer) to fill
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_set_lens_iris_table(ctrl_protocol_handle_t protocol,
                                      ctrl_channel_handle_t channel, int num, int32_t *values);

/**
 * @brief Gets the fine iris enable status
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[out]  enable   pointer to returned enable flag
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_get_lens_fine_iris(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                     uint8_t *enable);

/**
 * @brief Enable or disable fine iris
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[out]  enable   enable flag
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_set_lens_fine_iris(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                     uint8_t enable);

/**
 * @brief Gets the filter position
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[out]  pos      pointer to position value
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_get_lens_filter_position(ctrl_protocol_handle_t protocol,
                                           ctrl_channel_handle_t channel, int32_t *act);

/**
 * @brief Sets filter position value
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[out]  pos      pointer to position value
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_set_lens_filter_position(ctrl_protocol_handle_t protocol,
                                           ctrl_channel_handle_t channel, int32_t pos);

/**
 * @brief Gets the current lens filter settings
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[in]   num      number of array/list items
 * @param[out]  values   array (memory pointer) to fill
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_get_lens_filter_settings(ctrl_protocol_handle_t protocol,
                                           ctrl_channel_handle_t channel, int num, int32_t *values);

/**
 * @brief Sets lens filter settings
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[in]   num      number of array/list items
 * @param[out]  values   array (memory pointer) to fill
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_set_lens_filter_settings(ctrl_protocol_handle_t protocol,
                                           ctrl_channel_handle_t channel, int num, int32_t *values);

/**
 * @brief Gets the lens mode
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[out]  mode     pointer where read lens mode is written to
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_get_lens_mode(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                uint16_t *mode);

/**
 * @brief Sets lens mode
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[in]   mode     lens mode value to set
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_set_lens_mode(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                uint16_t mode);

/**
 * @brief Gets the motor offset
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[out]  offset   pointer where read motor offset is written to
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_get_motor_offset(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                   uint16_t *offset);

/**
 * @brief Sets motor offset
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[in]   offset   motor offset value to set
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_set_motor_offset(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                   uint16_t offset);

/**
 * @brief Gets the focus offset
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[out]  offset   pointer where read focus offset is written to
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_get_focus_offset(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                   uint16_t *offset);

/**
 * @brief Sets focus offset
 *
 * @param[in]   channel  control channel instance
 * @param[in]   protocol control protocol instance
 * @param[in]   offset   focus offset value to set
 *
 * @return      0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_set_focus_offset(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                   uint16_t offset);

/**
 * @brief ctrl_protocol_get_b4_info
 * @param[in]  protocol control channel instance
 * @param[in]  channel  control protocol instance
 * @param[out] info     info string
 * @return 0 on success. error-code otherwise
 *****************************************************************************/
int ctrl_protocol_get_lens_b4_info(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                   int info_type, uint8_t *info);

/**
 * @brief ctrl_protocol_get_motor_rotating
 * @param[in] protocol   control channel instance
 * @param[in] channel    control protocol instance
 * @param[out] rotating  is at least one motor rotating
 * @return 0 on success. error-code otherwise
 *****************************************************************************/
int ctrl_protocol_get_motor_rotating(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                     uint8_t *rotating);

typedef struct ctrl_protocol_lens_drv_s
{
    ctrl_protocol_int32_array_t get_lens_settings;
    ctrl_protocol_int32_array_t set_lens_settings;
    ctrl_protocol_get_int32_t get_lens_active;
    ctrl_protocol_set_int32_t set_lens_active;

    ctrl_protocol_int32_array_t get_lens_invert;
    ctrl_protocol_int32_array_t set_lens_invert;
    ctrl_protocol_get_uint8_t get_lens_auto_torque;
    ctrl_protocol_set_uint8_t set_lens_auto_torque;

    ctrl_protocol_get_int32_t get_lens_focus_motor_position;
    ctrl_protocol_set_int32_t set_lens_focus_motor_position;
    ctrl_protocol_get_int32_t get_lens_focus_position;
    ctrl_protocol_set_int32_t set_lens_focus_position;
    ctrl_protocol_int32_array_t get_lens_focus_settings;
    ctrl_protocol_int32_array_t set_lens_focus_settings;
    ctrl_protocol_get_uint8_t get_lens_fine_focus;
    ctrl_protocol_set_uint8_t set_lens_fine_focus;

    ctrl_protocol_get_int32_t get_lens_zoom_position;
    ctrl_protocol_set_int32_t set_lens_zoom_position;
    ctrl_protocol_get_int32_t get_lens_zoom_direction;
    ctrl_protocol_set_int32_t set_lens_zoom_direction;
    ctrl_protocol_int32_array_t get_lens_zoom_settings;
    ctrl_protocol_int32_array_t set_lens_zoom_settings;
    ctrl_protocol_get_uint8_t get_lens_fine_zoom;
    ctrl_protocol_set_uint8_t set_lens_fine_zoom;

    ctrl_protocol_get_int32_t get_lens_iris_position;
    ctrl_protocol_set_int32_t set_lens_iris_position;
    ctrl_protocol_int32_array_t get_lens_iris_settings;
    ctrl_protocol_int32_array_t set_lens_iris_settings;
    ctrl_protocol_get_int32_t get_lens_iris_aperture;
    ctrl_protocol_set_int32_t set_lens_iris_aperture;
    ctrl_protocol_int32_array_t get_lens_iris_table;
    ctrl_protocol_int32_array_t set_lens_iris_table;
    ctrl_protocol_get_uint8_t get_lens_fine_iris;
    ctrl_protocol_set_uint8_t set_lens_fine_iris;

    ctrl_protocol_get_int32_t get_lens_filter_position;
    ctrl_protocol_set_int32_t set_lens_filter_position;
    ctrl_protocol_int32_array_t get_lens_filter_settings;
    ctrl_protocol_int32_array_t set_lens_filter_settings;

    ctrl_protocol_get_uint16_t get_lens_mode;
    ctrl_protocol_set_uint16_t set_lens_mode;
    ctrl_protocol_get_uint16_t get_motor_offset;
    ctrl_protocol_set_uint16_t set_motor_offset;
    ctrl_protocol_get_uint16_t get_focus_offset;
    ctrl_protocol_set_uint16_t set_focus_offset;

    ctrl_protocol_uint8_array_t get_lens_b4_info;

    ctrl_protocol_get_uint8_t get_motor_rotating;
} ctrl_protocol_lens_drv_t;

/**
 * @brief      Register a protocol implementation at control protocol layer
 *
 * @param      handle   instance handle of control protocol layer
 * @param      ctx      private context of protocol driver implementation
 * @param      drv      driver functions of protocol implementation
 *
 * @return     0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_lens_register(ctrl_protocol_handle_t handle, void *ctx,
                                const ctrl_protocol_lens_drv_t *drv);

/**
 * @brief      Remove/unregister a protocol implementation from
 *             control protocol layer
 *
 * @param      handle   instance handle of control protocol layer
 *
 * @return     0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_lens_unregister(ctrl_protocol_handle_t handle);

/* @} ctrl_protocol_layer */

#ifdef __cplusplus
}
#endif

#endif /* CTRL_PROTOCOL_LENS_H */
