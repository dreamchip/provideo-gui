/*
 * Copyright (C) 2017 Dream Chip Technologies GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
#include <math.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <limits.h>

#include <simple_math/float.h>
#include <simple_math/knee.h>

/* Tone Map function (Knee function):
 *
 * f(x) = x                     for x <= x0
 * f(x) = a * sqrt(x+c) - b     for x > x0
 *
 * The solution for a, b and c is not trivial, details can be found here:
 * http://dctsvn01/repos/provideo/trunk/doc/research/Knee Function Research.xlsx */

/**< Macro to validate the passed handle */
#define CHECK_KIP_HANDLE(handle)                                                                   \
    {                                                                                              \
        if (NULL == (handle)) {                                                                    \
            return (-EFAULT);                                                                      \
        }                                                                                          \
    }

/**< Macro to validate the current state */
#define CHECK_KIP_STATE(ctx, value)                                                                \
    {                                                                                              \
        if (((ctx) == NULL) || ((ctx)->state != (value))) {                                        \
            return (-EFAULT);                                                                      \
        }                                                                                          \
    }

/*
 * local variables
 *****************************************************************************/
static knee_interpolation_ctx_t sKnee_interpol_ctx;

/*
 * sm_knee_interpolation_init
 *****************************************************************************/
int sm_knee_interpolation_init(knee_interpolation_ctx_t **ctx)
{
    memset(&sKnee_interpol_ctx, 0, sizeof(sKnee_interpol_ctx)); // NOLINT
    sKnee_interpol_ctx.state = KIP_STATE_INITIALIZED;
    *ctx = &sKnee_interpol_ctx;
    return (0);
}

/*
 * sm_knee_interpolation_reset
 *****************************************************************************/
int sm_knee_interpolation_reset(knee_interpolation_ctx_t *const ctx)
{
    CHECK_KIP_HANDLE(ctx);
    memset(ctx, 0, sizeof(sKnee_interpol_ctx)); // NOLINT
    ctx->state = KIP_STATE_INITIALIZED;
    return (0);
}

/*
 * sm_knee_interpolation_calc_init
 *****************************************************************************/
int sm_knee_interpolation_calc_init(knee_interpolation_ctx_t *const ctx, uint8_t const knee,
                                    uint16_t const slope)
{
    float x0 = NAN, tan_slope = NAN, tan_slope_squared = NAN;

    CHECK_KIP_HANDLE(ctx);
    CHECK_KIP_STATE(ctx, KIP_STATE_INITIALIZED);

    // range check knee position
    if (knee > 100) {
        return (-EINVAL);
    }

    // range check slope
    if (slope > 90) {
        return (-EINVAL);
    }

    /* Apply inverse gamma function on the knee position since the user will
     * adjust knee while looking on his monitor which will show him the SDI code
     * values which have the gamma function applied. */
    /* FIXME: We use the inverse gamma function for the default Rec.709 gamma curve
     * with a gamma value of 0.22, for all other gamma functions this is only an
     * approximation! */
    x0 = fastpow(((((float)knee / 100.0f) + 0.099f) / 1.099f), (1.0f / 0.45f));
    clip(x0, 0.0f, 1.0f);
    ctx->x0 = x0;
    ctx->s = slope;

    // Calculate the a, b and c values for the tone map function
    if (slope == 45 || slope == 90) {
        // In these cases f(x) is not defined, this must also be caught in sm_knee_gain_function()!
        ctx->a = 0.0f;
        ctx->b = 0.0f;
        ctx->c = 0.0f;

        goto exit;
    } else if (slope < 45) {
        tan_slope = tan_f((float)slope * M_PI / 180.0f);
    } else /* if ( slope > 45 ) */
    {
        tan_slope = tan_f((float)(90 - slope) * M_PI / 180.0f);
    }
    tan_slope_squared = tan_slope * tan_slope;

    ctx->a = -((x0 - 1.0f) * tan_slope) / fastpow(x0 * tan_slope - tan_slope - x0 + 1.0f, 0.5f);
    ctx->b = (x0 * tan_slope_squared - 2.0f * x0 * tan_slope - tan_slope_squared + 2.0f * x0)
            / (2.0f * (tan_slope - 1.0f));
    ctx->c = (x0 * tan_slope_squared - 4.0f * x0 * tan_slope - tan_slope_squared + 4.0f * x0)
            / (4.0f * (tan_slope - 1.0f));

exit:
    ctx->state = KIP_STATE_RUNNING;

    return (0);
}

/*
 * sm_knee_interpolation_calc
 *****************************************************************************/
int sm_knee_interpolation_calc(knee_interpolation_ctx_t *const ctx, uint32_t const x,
                               uint32_t *const gain, uint8_t const bit_width_input,
                               uint8_t const bit_width_output)
{
    float Vin = (float)x;
    float Vout = 0.0f;

    CHECK_KIP_HANDLE(ctx);
    CHECK_KIP_STATE(ctx, KIP_STATE_RUNNING);

    uint32_t size_i = (1ul << bit_width_input);
    uint32_t size_o = (1ul << bit_width_output);

    if (!ctx || !gain) {
        return (-EINVAL);
    }

    Vin /= (float)size_i; // normalize to range 0..1

    // For the linear part and for corner cases of s and x0 use gain 1
    if (Vin < ctx->x0 || ctx->s == 45 || ctx->x0 >= 1.0f) {
        Vout = 1.0f;
    }
    // 90° means infinitely steep curve, so use maximum int value
    else if (ctx->s == 90) {
        Vout = (float)UINT_MAX;
    }
    // Highlights are being limited -> calculate normal knee function
    else if (ctx->s < 45) {
        // f(x) = a * sqrt(x+c) - b
        Vout = Vin;
        Vout += ctx->c;
        Vout = fastpow(Vout, 0.5f);
        Vout *= ctx->a;
        Vout -= ctx->b;
        Vout /= Vin;
    }
    // Highlights are being increased -> calculate mirrored knee function
    else // if ( ctx->s > 45 )
    {
        // f(x) = ((x+b)/a)^2 - c
        Vout = Vin;
        Vout += ctx->b;
        Vout /= ctx->a;
        Vout = fastpow(Vout, 2.0f);
        Vout -= ctx->c;
        Vout /= Vin;
    }

    Vout *= (float)size_o; // normalize to output bit-width
    Vout += 0.5f; // for rounding

    // Make sure that Vout is not bigger than the maximum of uint32
    if (Vout > (float)UINT_MAX) {
        *gain = UINT_MAX;
    } else {
        *gain = (uint32_t)Vout;
    }

    return (0);
}
