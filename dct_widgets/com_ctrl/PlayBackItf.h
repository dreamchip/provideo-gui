/*
 * Copyright (C) 2017 Dream Chip Technologies GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
/**
 * @file    PlayBackItf.h
 *
 * @brief   Playback Interface
 *
 * @note    Multiple Inheritance of an QObject is not allowed.
 *
 *****************************************************************************/
#ifndef PLAYBACK_INTERFACE_H
#define PLAYBACK_INTERFACE_H

#include <QObject>
#include <QVector>

#include "ProVideoItf.h"

class PlayBackItf : public ProVideoItf
{
    Q_OBJECT

public:
    explicit PlayBackItf(ComChannel *c, ComProtocol *p) : ProVideoItf(c, p) { }

    // resync all settings
    void resync() override;

    // current record buffer
    void GetRec();

    // record mode
    void GetRecMode();

    // current playback buffer
    void GetPlay();

    // play mode
    void GetPlayMode();

    // stop mode
    void GetStopMode();

    // current position in playback buffer
    void GetPos();

    // mark in and mark out positions
    void GetMarkPos();

    // buffer status
    void GetStatus();

    // buffer count
    void GetCount();

    // SSD status
    void GetHealth();

signals:
    void RecChanged(int buffer_id);
    void RecModeChanged(int mode);

    void PlayChanged(int buffer_id, int speed);
    void PlayModeChanged(int mode);

    void StopModeChanged(int mode);

    void PosChanged(int pos);
    void MarkPosChanged(int mark_in, int mark_out);

    void StatusChanged(int buffer_id, int num_frames, int max_frames, QString status);
    void CountChanged(int num);

    void HealthChanged(int ssd_id, int status_code, QString status_string, int temp_min, int temp,
                       int temp_max, int lifetime);

public slots:
    void onRecChange(int buffer_id);
    void onRecModeChange(int mode);
    void onRecStop();

    void onPlayChange(int buffer_id, int speed);
    void onPlayModeChange(int mode);
    void onPause();

    void onStop();
    void onStopModeChange(int mode);

    void onSeekChange(int mode, int pos);
    void onMarkIn(int pos);
    void onMarkOut(int pos);

    void onFreeChange(int buffer_id);
    void onCountChange(int num);

    void onUpdateStatus();
    void onUpdatePos();
    void onUpdateHealth();
};

#endif // PLAYBACK_INTERFACE_H
