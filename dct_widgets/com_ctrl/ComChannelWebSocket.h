/*
 * Copyright (C) 2021 Dream Chip Technologies GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
/**
 * @file    ComChannelWebSocket.h
 *
 * @brief   ComChannel, that is using WebSocket to communicate with a server.
 *
 *****************************************************************************/
#ifndef COM_CHANNEL_WEB_SOCKET_H
#define COM_CHANNEL_WEB_SOCKET_H

#include <QtWebSockets/QWebSocket>
#include <QTimer>
#include <QObject>

#include "ComChannel.h"

// generic control channel layer
#include <ctrl_channel/ctrl_channel_serial.h>

class ComChannelWebSocket : public ComChannel
{
    Q_OBJECT

public:
    explicit ComChannelWebSocket(QString address = "", uint16_t port = 0, uint8_t dev_addr = 0);
    ~ComChannelWebSocket() override;

    bool IsSerial() override { return false; }

    void ReOpen() override;

    QString getAddress() { return m_address; }
    void setAddress(const QString &text) { m_address = text; }

    uint16_t getPort() const { return m_port; }
    void setPort(uint16_t value) { m_port = value; }

    uint8_t getDeviceAddress() const { return m_dev_addr; }
    void setDeviceAddress(uint8_t dev_addr) { m_dev_addr = dev_addr; }

    // declarations of functions, that are implementing the ctrl_channel interface
    static int ctrl_channel_websocket_get_no_ports(void *handle);
    static int ctrl_channel_websocket_get_port_name(void *handle, int idx,
                                                    ctrl_channel_name_t name);
    static int ctrl_channel_websocket_open(void *handle, void *param, int size);
    static int ctrl_channel_websocket_close(void *handle);
    static int ctrl_channel_websocket_send_request(void *handle, uint8_t *data, int len);
    static int ctrl_channel_websocket_get_response(void *handle, uint8_t *data, int len);

    void emitDataReceived(char const *data);

Q_SIGNALS:
    void closed();

public slots:
    void onSendData(QString data, int responseWaitTime) override;

private slots:
    void onConnected();
    void onBinaryMessageReceived(const QByteArray &message);
    void onTextMessageReceived(const QString &message);
    void onDisConnected();
    void onTimeout();

private: // NOLINT(readability-redundant-access-specifiers)
    QString m_address;
    uint16_t m_port;

    uint8_t m_dev_addr;

    QWebSocket m_webSocket;
    QString m_lastMessage;

    QTimer *m_timer;
    bool m_timed_out;

    bool m_emit_data;
};

#endif // COM_CHANNEL_WEB_SOCKET_H
