/*
 * Copyright (C) 2017 Dream Chip Technologies GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
/**
 * @file    LensItf.cpp
 *
 * @brief   Implementation of Lens Control Interface
 *
 *****************************************************************************/
#include <cerrno>
#include <cstring>

#include <ctrl_protocol/ctrl_protocol_lens.h>

#include "common.h"
#include "LensItf.h"

#include <QtDebug>

#include <array>

using namespace std;

/**
 * @brief LensItf::resync()
 *****************************************************************************/
void LensItf::resync()
{
    // Note: Get settings before getting the active motors so that the lens
    // driver box can correctly evaluate if the iris motor is active.
    GetLensSettings();
    GetLensActive();
    GetLensInvert();
    GetLensAutoTorque();

    // Get table before getting iris position so that the GUI already knows
    // the position -> aperture mapping and can display it correctly
    GetLensIrisTable();

    // Read "fine" settings before reading the positions to correctly display the ranges
    GetLensFocusFine();
    GetLensZoomFine();
    GetLensIrisFine();

    GetLensFocusPosition();
    GetLensZoomPosition();
    GetLensIrisPosition();
    GetLensFilterPosition();

    GetLensZoomDirection();

    GetLensFocusSettings();
    GetLensZoomSettings();
    GetLensIrisSettings();
    GetLensFilterSettings();

    GetLensMode();
    GetLensMotorOffset();
    GetLensFocusOffset();
}

/**
 * @brief LensItf::GetLensSettings
 *****************************************************************************/
void LensItf::GetLensSettings()
{
    std::array<int32_t, NO_VALUES_LENS_SETTINGS> value = {};

    // get lens settings from device
    auto res =
            ctrl_protocol_get_lens_settings(GET_PROTOCOL_INSTANCE(this), GET_CHANNEL_INSTANCE(this),
                                            NO_VALUES_LENS_SETTINGS, value.data());
    HANDLE_ERROR(res);

    QVector<int> v_values(NO_VALUES_LENS_SETTINGS);
    for (int i = 0; i < NO_VALUES_LENS_SETTINGS; i++) {
        v_values[i] = value.at(i);
    }

    if (receivers(SIGNAL(LensSettingsChanged(QVector<int>))) > 0) {
        emit LensSettingsChanged(v_values);
    }
}

/**
 * @brief LensItf::GetLensActive
 *****************************************************************************/
void LensItf::GetLensActive()
{
    int32_t value = 0;

    // get lens settings from device
    auto res = ctrl_protocol_get_lens_active(GET_PROTOCOL_INSTANCE(this),
                                             GET_CHANNEL_INSTANCE(this), &value);

    HANDLE_ERROR(res);

    if (receivers(SIGNAL(LensActiveChanged(int))) > 0) {
        emit LensActiveChanged(value);
    }
}

/**
 * @brief LensItf::GetLensSettings
 *****************************************************************************/
void LensItf::GetLensInvert()
{
    std::array<int32_t, NO_VALUES_LENS_INVERT> value = {};

    // get lens invert values from device
    auto res =
            ctrl_protocol_get_lens_invert(GET_PROTOCOL_INSTANCE(this), GET_CHANNEL_INSTANCE(this),
                                          NO_VALUES_LENS_INVERT, value.data());
    HANDLE_ERROR(res);

    QVector<int> v_values(NO_VALUES_LENS_INVERT);
    for (int i = 0; i < NO_VALUES_LENS_INVERT; i++) {
        v_values[i] = value.at(i);
    }

    if (receivers(SIGNAL(LensInvertChanged(QVector<int>))) > 0) {
        emit LensInvertChanged(v_values);
    }
}

/**
 * @brief LensItf::GetLensAutoTorque
 *****************************************************************************/
void LensItf::GetLensAutoTorque()
{
    uint8_t enable = 0;

    // get lens settings from device
    auto res = ctrl_protocol_get_lens_auto_torque(GET_PROTOCOL_INSTANCE(this),
                                                  GET_CHANNEL_INSTANCE(this), &enable);
    HANDLE_ERROR(res);

    if (receivers(SIGNAL(LensAutoTorqueChanged(bool))) > 0) {
        emit LensAutoTorqueChanged((bool)enable);
    }
}

/**
 * @brief LensItf::GetLensFocusFine
 *****************************************************************************/
void LensItf::GetLensFocusFine()
{
    uint8_t value = 0;

    // get lens settings from device
    auto res = ctrl_protocol_get_lens_fine_focus(GET_PROTOCOL_INSTANCE(this),
                                                 GET_CHANNEL_INSTANCE(this), &value);
    HANDLE_ERROR(res);

    if (receivers(SIGNAL(LensFocusFineChanged(bool))) > 0) {
        emit LensFocusFineChanged((bool)value);
    }
}

/**
 * @brief LensItf::GetLensZoomFine
 *****************************************************************************/
void LensItf::GetLensZoomFine()
{
    uint8_t value = 0;

    // get lens settings from device
    auto res = ctrl_protocol_get_lens_fine_zoom(GET_PROTOCOL_INSTANCE(this),
                                                GET_CHANNEL_INSTANCE(this), &value);
    HANDLE_ERROR(res);

    if (receivers(SIGNAL(LensZoomFineChanged(bool))) > 0) {
        emit LensZoomFineChanged((bool)value);
    }
}

/**
 * @brief LensItf::GetLensIrisFine
 *****************************************************************************/
void LensItf::GetLensIrisFine()
{
    uint8_t value = 0;

    // get lens settings from device
    auto res = ctrl_protocol_get_lens_fine_iris(GET_PROTOCOL_INSTANCE(this),
                                                GET_CHANNEL_INSTANCE(this), &value);
    HANDLE_ERROR(res);

    if (receivers(SIGNAL(LensIrisFineChanged(bool))) > 0) {
        emit LensIrisFineChanged((bool)value);
    }
}

/**
 * @brief LensItf::GetLensFocusPosition
 *****************************************************************************/
void LensItf::GetLensFocusPosition()
{
    int32_t value = 0;

    // get lens settings from device
    auto res = ctrl_protocol_get_lens_focus_position(GET_PROTOCOL_INSTANCE(this),
                                                     GET_CHANNEL_INSTANCE(this), &value);
    HANDLE_ERROR(res);

    if (receivers(SIGNAL(LensFocusPositionChanged(int))) > 0) {
        emit LensFocusPositionChanged(value);
    }
}

/**
 * @brief LensItf::GetLensFocusMotorPosition
 *****************************************************************************/
int32_t LensItf::GetLensFocusMotorPosition()
{
    int32_t value = 0;

    // get lens settings from device
    auto res = ctrl_protocol_get_lens_focus_motor_position(GET_PROTOCOL_INSTANCE(this),
                                                           GET_CHANNEL_INSTANCE(this), &value);
    if (res != 0) {
        showError(res, static_cast<const char *>(__FILE__), static_cast<const char *>(__FUNCTION__),
                  __LINE__);
        return value;
    }

    if (receivers(SIGNAL(LensFocusMotorPositionChanged(int))) > 0) {
        emit LensFocusMotorPositionChanged(value);
    }
    return value;
}

/**
 * @brief LensItf::GetLensZoomPosition
 *****************************************************************************/
void LensItf::GetLensZoomPosition()
{
    int32_t value = 0;

    // get lens settings from device
    auto res = ctrl_protocol_get_lens_zoom_position(GET_PROTOCOL_INSTANCE(this),
                                                    GET_CHANNEL_INSTANCE(this), &value);
    HANDLE_ERROR(res);

    if (receivers(SIGNAL(LensZoomPositionChanged(int))) > 0) {
        emit LensZoomPositionChanged(value);
    }
}

/**
 * @brief LensItf::GetLensIrisPosition
 *****************************************************************************/
void LensItf::GetLensIrisPosition()
{
    int32_t value = 0;

    // get lens settings from device
    auto res = ctrl_protocol_get_lens_iris_position(GET_PROTOCOL_INSTANCE(this),
                                                    GET_CHANNEL_INSTANCE(this), &value);
    HANDLE_ERROR(res);

    if (receivers(SIGNAL(LensIrisPositionChanged(int))) > 0) {
        emit LensIrisPositionChanged(value);
    }
}

/**
 * @brief LensItf::GetLensZoomDirection
 *****************************************************************************/
void LensItf::GetLensZoomDirection()
{
    int32_t value = 0;

    // get lens settings from device
    auto res = ctrl_protocol_get_lens_zoom_direction(GET_PROTOCOL_INSTANCE(this),
                                                     GET_CHANNEL_INSTANCE(this), &value);
    HANDLE_ERROR(res);

    if (receivers(SIGNAL(LensZoomDirectionChanged(int))) > 0) {
        emit LensZoomDirectionChanged(value);
    }
}

/**
 * @brief LensItf::GetLensIrisAperture
 *****************************************************************************/
void LensItf::GetLensIrisAperture()
{
    int32_t value = 0;

    // get lens settings from device
    auto res = ctrl_protocol_get_lens_iris_aperture(GET_PROTOCOL_INSTANCE(this),
                                                    GET_CHANNEL_INSTANCE(this), &value);
    HANDLE_ERROR(res);

    if (receivers(SIGNAL(LensIrisApertureChanged(int))) > 0) {
        emit LensIrisApertureChanged(value);
    }
}

/**
 * @brief LensItf::GetLensFilterPosition
 *****************************************************************************/
void LensItf::GetLensFilterPosition()
{
    int32_t value = 0;

    // get lens settings from device
    auto res = ctrl_protocol_get_lens_filter_position(GET_PROTOCOL_INSTANCE(this),
                                                      GET_CHANNEL_INSTANCE(this), &value);
    HANDLE_ERROR(res);

    if (receivers(SIGNAL(LensFilterPositionChanged(int))) > 0) {
        emit LensFilterPositionChanged(value);
    }
}

/**
 * @brief LensItf::GetLensFocusSettings
 *****************************************************************************/
void LensItf::GetLensFocusSettings()
{
    std::array<int32_t, NO_VALUES_LENS_FOCUS_SETTINGS> value = {};

    // get lens settings from device
    auto res = ctrl_protocol_get_lens_focus_settings(GET_PROTOCOL_INSTANCE(this),
                                                     GET_CHANNEL_INSTANCE(this),
                                                     NO_VALUES_LENS_FOCUS_SETTINGS, value.data());
    HANDLE_ERROR(res);

    if (receivers(SIGNAL(LensFocusSettingsChanged(QVector<int>))) > 0) {
        QVector<int> qValues;

        qValues.append(value[0]);
        qValues.append(value[1]);
        qValues.append(value[2]);

        emit LensFocusSettingsChanged(qValues);
    }
}

/**
 * @brief LensItf::GetLensZoomSettings
 *****************************************************************************/
void LensItf::GetLensZoomSettings()
{
    std::array<int32_t, NO_VALUES_LENS_ZOOM_SETTINGS> value = {};

    // get lens settings from device
    auto res = ctrl_protocol_get_lens_zoom_settings(GET_PROTOCOL_INSTANCE(this),
                                                    GET_CHANNEL_INSTANCE(this),
                                                    NO_VALUES_LENS_ZOOM_SETTINGS, value.data());
    HANDLE_ERROR(res);

    if (receivers(SIGNAL(LensZoomSettingsChanged(QVector<int>))) > 0) {
        QVector<int> qValues;

        qValues.append(value[0]);
        qValues.append(value[1]);
        qValues.append(value[2]);

        emit LensZoomSettingsChanged(qValues);
    }
}

/**
 * @brief LensItf::GetLensIrisSettings
 *****************************************************************************/
void LensItf::GetLensIrisSettings()
{
    std::array<int32_t, NO_VALUES_LENS_IRIS_SETTINGS> value = {};

    // get lens settings from device
    auto res = ctrl_protocol_get_lens_iris_settings(GET_PROTOCOL_INSTANCE(this),
                                                    GET_CHANNEL_INSTANCE(this),
                                                    NO_VALUES_LENS_IRIS_SETTINGS, value.data());
    HANDLE_ERROR(res);

    if (receivers(SIGNAL(LensIrisSettingsChanged(QVector<int>))) > 0) {
        QVector<int> qValues;

        qValues.append(value[0]);
        qValues.append(value[1]);
        qValues.append(value[2]);

        emit LensIrisSettingsChanged(qValues);
    }
}

/**
 * @brief LensItf::GetLensIrisTable
 *****************************************************************************/
void LensItf::GetLensIrisTable()
{
    std::array<int32_t, NO_VALUES_LENS_IRIS_TABLE> value = {};

    // get lens settings from device
    auto res = ctrl_protocol_get_lens_iris_table(GET_PROTOCOL_INSTANCE(this),
                                                 GET_CHANNEL_INSTANCE(this),
                                                 NO_VALUES_LENS_IRIS_TABLE, value.data());
    HANDLE_ERROR(res);

    if (receivers(SIGNAL(LensIrisTableChanged(QVector<int>))) > 0) {
        QVector<int> qValues;

        for (int i = 0; i < NO_VALUES_LENS_IRIS_TABLE; i++) {
            qValues.append(value.at(i));
        }

        emit LensIrisTableChanged(qValues);
    }
}

/**
 * @brief LensItf::GetLensFilterSettings
 *****************************************************************************/
void LensItf::GetLensFilterSettings()
{
    std::array<int32_t, NO_VALUES_LENS_FILTER_SETTINGS> value = {};

    // get lens settings from device
    auto res = ctrl_protocol_get_lens_filter_settings(GET_PROTOCOL_INSTANCE(this),
                                                      GET_CHANNEL_INSTANCE(this),
                                                      NO_VALUES_LENS_FILTER_SETTINGS, value.data());
    HANDLE_ERROR(res);

    if (receivers(SIGNAL(LensFilterSettingsChanged(QVector<int>))) > 0) {
        QVector<int> qValues;

        qValues.append(value[0]);
        qValues.append(value[1]);
        qValues.append(value[2]);

        emit LensFilterSettingsChanged(qValues);
    }
}

/**
 * @brief LensItf::GetLensMode
 *****************************************************************************/
void LensItf::GetLensMode()
{
    uint16_t value = 0;

    // get lens mode from device
    auto res = ctrl_protocol_get_lens_mode(GET_PROTOCOL_INSTANCE(this), GET_CHANNEL_INSTANCE(this),
                                           &value);
    HANDLE_ERROR(res);

    if (receivers(SIGNAL(LensModeChanged(int))) > 0) {
        emit LensModeChanged((int)value);
    }
}

/**
 * @brief LensItf::GetLensMotorOffset
 *****************************************************************************/
uint16_t LensItf::GetLensMotorOffset()
{
    uint16_t value = 0;

    // get lens motor offset from device
    auto res = ctrl_protocol_get_motor_offset(GET_PROTOCOL_INSTANCE(this),
                                              GET_CHANNEL_INSTANCE(this), &value);
    if (res != 0) {
        showError(res, static_cast<const char *>(__FILE__), static_cast<const char *>(__FUNCTION__),
                  __LINE__);
        return value;
    }
    if (receivers(SIGNAL(LensModeChanged(int))) > 0) {
        emit LensMotorOffsetChanged((int)value);
    }
    return value;
}

/**
 * @brief LensItf::GetLensFocusOffset
 *****************************************************************************/
uint16_t LensItf::GetLensFocusOffset()
{
    uint16_t value = 0;

    // get lens focus offset from device
    auto res = ctrl_protocol_get_focus_offset(GET_PROTOCOL_INSTANCE(this),
                                              GET_CHANNEL_INSTANCE(this), &value);
    if (res != 0) {
        showError(res, static_cast<const char *>(__FILE__), static_cast<const char *>(__FUNCTION__),
                  __LINE__);
        return value;
    }

    if (receivers(SIGNAL(LensModeChanged(int))) > 0) {
        emit LensFocusOffsetChanged((int)value);
    }
    return value;
}

/**
 * @brief Sends "motor_rotating" command to the camera and emits a Qt signal.
 * @return 0 if OK, otherwise an error-code
 *****************************************************************************/
void LensItf::GetMotorRotating()
{
    uint8_t value = 0;

    // get lens motor rotating from device
    auto res = ctrl_protocol_get_motor_rotating(GET_PROTOCOL_INSTANCE(this),
                                                GET_CHANNEL_INSTANCE(this), &value);
    HANDLE_ERROR(res);

    if (receivers(SIGNAL(LensMotorRotatingChanged(bool))) > 0) {
        emit LensMotorRotatingChanged(value != 0);
    }
}

/**
 * @brief LensItf::onLensSettingsChange
 *****************************************************************************/
void LensItf::onLensSettingsChange(QVector<int> values)
{
    std::array<int, NO_VALUES_LENS_SETTINGS> value_array = {};
    for (int i = 0; i < NO_VALUES_LENS_SETTINGS; i++) {
        value_array.at(i) = values[i];
    }

    auto res =
            ctrl_protocol_set_lens_settings(GET_PROTOCOL_INSTANCE(this), GET_CHANNEL_INSTANCE(this),
                                            NO_VALUES_LENS_SETTINGS, value_array.data());
    HANDLE_ERROR(res);

    // Update motor setup of selected motors
    if (values[3] != 0) {
        GetLensFocusSettings();
        GetLensFocusFine();
        GetLensFocusPosition();
    }
    if (values[4] != 0) {
        GetLensZoomSettings();
        GetLensZoomFine();
        GetLensZoomPosition();
    }
    if (values[5] != 0) {
        GetLensIrisSettings();
        GetLensIrisTable();
        GetLensIrisPosition();
    }
    if (values[6] != 0) {
        GetLensFilterSettings();
        GetLensFilterPosition();
    }

    GetLensInvert();
}

/**
 * @brief LensItf::onLensActiveChange
 *****************************************************************************/
void LensItf::onLensActiveChange(int active)
{
    auto res = ctrl_protocol_set_lens_active(GET_PROTOCOL_INSTANCE(this),
                                             GET_CHANNEL_INSTANCE(this), active);

    if (res != 0) {
        // emit signal for indicating that no lens driver was found
        emit LensActiveChanged(-1);
    } else {
        // Lens driver was found, update all settings
        resync();
    }
}

/**
 * @brief LensItf::onLensInvertChange
 *****************************************************************************/
void LensItf::onLensInvertChange(QVector<int> values)
{
    std::array<int, NO_VALUES_LENS_INVERT> value_array = {};
    for (int i = 0; i < NO_VALUES_LENS_INVERT; i++) {
        value_array.at(i) = values[i];
    }

    auto res =
            ctrl_protocol_set_lens_invert(GET_PROTOCOL_INSTANCE(this), GET_CHANNEL_INSTANCE(this),
                                          NO_VALUES_LENS_INVERT, value_array.data());
    HANDLE_ERROR(res);

    // Update positions as they might got inverted
    GetLensFocusPosition();
    GetLensZoomPosition();
    GetLensIrisPosition();
    GetLensFilterPosition();
}

/**
 * @brief LensItf::onLensAutoTorqueChange
 *****************************************************************************/
void LensItf::onLensAutoTorqueChange(bool enable)
{
    auto res = ctrl_protocol_set_lens_auto_torque(GET_PROTOCOL_INSTANCE(this),
                                                  GET_CHANNEL_INSTANCE(this), (uint8_t)enable);

    HANDLE_ERROR(res);
}

/**
 * @brief LensItf::onLensFocusFineChange
 *****************************************************************************/
void LensItf::onLensFocusFineChange(bool enable)
{
    auto res = ctrl_protocol_set_lens_fine_focus(GET_PROTOCOL_INSTANCE(this),
                                                 GET_CHANNEL_INSTANCE(this), enable ? 1 : 0);

    HANDLE_ERROR(res);
}

/**
 * @brief LensItf::onLensZoomFineChange
 *****************************************************************************/
void LensItf::onLensZoomFineChange(bool enable)
{
    auto res = ctrl_protocol_set_lens_fine_zoom(GET_PROTOCOL_INSTANCE(this),
                                                GET_CHANNEL_INSTANCE(this), enable ? 1 : 0);

    HANDLE_ERROR(res);
}

/**
 * @brief LensItf::onLensIrisFineChange
 *****************************************************************************/
void LensItf::onLensIrisFineChange(bool enable)
{
    auto res = ctrl_protocol_set_lens_fine_iris(GET_PROTOCOL_INSTANCE(this),
                                                GET_CHANNEL_INSTANCE(this), enable ? 1 : 0);

    HANDLE_ERROR(res);

    // need to get the new IrisTable
    GetLensIrisTable();
}

/**
 * @brief LensItf::onLensFocusPositionChange
 *****************************************************************************/
void LensItf::onLensFocusPositionChange(int value)
{
    auto res = ctrl_protocol_set_lens_focus_position(GET_PROTOCOL_INSTANCE(this),
                                                     GET_CHANNEL_INSTANCE(this), value);

    HANDLE_ERROR(res);
}

/**
 * @brief LensItf::onLensFocusMotorPositionChange
 *****************************************************************************/
void LensItf::onLensFocusMotorPositionChange(int value)
{
    auto res = ctrl_protocol_set_lens_focus_motor_position(GET_PROTOCOL_INSTANCE(this),
                                                           GET_CHANNEL_INSTANCE(this), value);

    HANDLE_ERROR(res);
}

/**
 * @brief LensItf::onLensZoomPositionChange
 *****************************************************************************/
void LensItf::onLensZoomPositionChange(int value)
{
    auto res = ctrl_protocol_set_lens_zoom_position(GET_PROTOCOL_INSTANCE(this),
                                                    GET_CHANNEL_INSTANCE(this), value);

    HANDLE_ERROR(res);
}

/**
 * @brief LensItf::onLensIrisPositionChange
 *****************************************************************************/
void LensItf::onLensIrisPositionChange(int value)
{
    auto res = ctrl_protocol_set_lens_iris_position(GET_PROTOCOL_INSTANCE(this),
                                                    GET_CHANNEL_INSTANCE(this), value);

    HANDLE_ERROR(res);
}

/**
 * @brief LensItf::onLensIrisApertureChange
 *****************************************************************************/
void LensItf::onLensIrisApertureChange(int value)
{
    auto res = ctrl_protocol_set_lens_iris_aperture(GET_PROTOCOL_INSTANCE(this),
                                                    GET_CHANNEL_INSTANCE(this), value);

    HANDLE_ERROR(res);
}

/**
 * @brief LensItf::onLensFilterPositionChange
 *****************************************************************************/
void LensItf::onLensFilterPositionChange(int value)
{
    auto res = ctrl_protocol_set_lens_filter_position(GET_PROTOCOL_INSTANCE(this),
                                                      GET_CHANNEL_INSTANCE(this), value);

    HANDLE_ERROR(res);
}

/**
 * @brief LensItf::onLensZoomDirectionChange
 *****************************************************************************/
void LensItf::onLensZoomDirectionChange(int value)
{
    auto res = ctrl_protocol_set_lens_zoom_direction(GET_PROTOCOL_INSTANCE(this),
                                                     GET_CHANNEL_INSTANCE(this), value);

    HANDLE_ERROR(res);
}

/**
 * @brief LensItf::onLensFocusSettingsChange
 *****************************************************************************/
void LensItf::onLensFocusSettingsChange(QVector<int> values)
{
    std::array<int, NO_VALUES_LENS_FOCUS_SETTINGS> value_array = {};

    value_array[0] = values[0];
    value_array[1] = values[1];
    value_array[2] = values[2];

    auto res = ctrl_protocol_set_lens_focus_settings(
            GET_PROTOCOL_INSTANCE(this), GET_CHANNEL_INSTANCE(this), NO_VALUES_LENS_FOCUS_SETTINGS,
            value_array.data());

    HANDLE_ERROR(res);
}

/**
 * @brief LensItf::onLensZoomSettingsChange
 *****************************************************************************/
void LensItf::onLensZoomSettingsChange(QVector<int> values)
{
    std::array<int, NO_VALUES_LENS_ZOOM_SETTINGS> value_array = {};

    value_array[0] = values[0];
    value_array[1] = values[1];
    value_array[2] = values[2];

    auto res = ctrl_protocol_set_lens_zoom_settings(
            GET_PROTOCOL_INSTANCE(this), GET_CHANNEL_INSTANCE(this), NO_VALUES_LENS_ZOOM_SETTINGS,
            value_array.data());

    HANDLE_ERROR(res);
}

/**
 * @brief LensItf::onLensIrisSettingsChange
 *****************************************************************************/
void LensItf::onLensIrisSettingsChange(QVector<int> values)
{
    std::array<int, NO_VALUES_LENS_IRIS_SETTINGS> value_array = {};

    value_array[0] = values[0];
    value_array[1] = values[1];
    value_array[2] = values[2];

    auto res = ctrl_protocol_set_lens_iris_settings(
            GET_PROTOCOL_INSTANCE(this), GET_CHANNEL_INSTANCE(this), NO_VALUES_LENS_IRIS_SETTINGS,
            value_array.data());

    HANDLE_ERROR(res);
}

/**
 * @brief LensItf::onLensIrisTableChange
 *****************************************************************************/
void LensItf::onLensIrisTableChange(QVector<int> values)
{
    if (values.length() > NO_VALUES_LENS_IRIS_TABLE) {
        HANDLE_ERROR(-ENOMEM);
    }

    std::array<int, NO_VALUES_LENS_IRIS_TABLE> value_array = {};
    for (int i = 0; i < values.length(); i++) {
        value_array.at(i) = values[i];
    }

    auto res = ctrl_protocol_set_lens_iris_table(GET_PROTOCOL_INSTANCE(this),
                                                 GET_CHANNEL_INSTANCE(this),
                                                 NO_VALUES_LENS_IRIS_TABLE, value_array.data());

    HANDLE_ERROR(res);
}

/**
 * @brief LensItf::onLensFilterSettingsChange
 *****************************************************************************/
void LensItf::onLensFilterSettingsChange(QVector<int> values)
{
    std::array<int, NO_VALUES_LENS_FILTER_SETTINGS> value_array = {};

    value_array[0] = values[0];
    value_array[1] = values[1];
    value_array[2] = values[2];

    auto res = ctrl_protocol_set_lens_filter_settings(
            GET_PROTOCOL_INSTANCE(this), GET_CHANNEL_INSTANCE(this), NO_VALUES_LENS_FILTER_SETTINGS,
            value_array.data());

    HANDLE_ERROR(res);
}

/**
 * @brief LensItf::onLensModeChange
 *****************************************************************************/
void LensItf::onLensModeChange(int mode)
{
    auto res = ctrl_protocol_set_lens_mode(GET_PROTOCOL_INSTANCE(this), GET_CHANNEL_INSTANCE(this),
                                           (uint16_t)mode);

    HANDLE_ERROR(res);
}

/**
 * @brief LensItf::onLensMotorOffsetChange
 *****************************************************************************/
void LensItf::onLensMotorOffsetChange(int offset)
{
    auto res = ctrl_protocol_set_motor_offset(GET_PROTOCOL_INSTANCE(this),
                                              GET_CHANNEL_INSTANCE(this), (uint16_t)offset);

    HANDLE_ERROR(res);
}

/**
 * @brief LensItf::onLensMotorOffsetChange
 *****************************************************************************/
void LensItf::onLensFocusOffsetChange(int offset)
{
    auto res = ctrl_protocol_set_focus_offset(GET_PROTOCOL_INSTANCE(this),
                                              GET_CHANNEL_INSTANCE(this), (uint16_t)offset);

    HANDLE_ERROR(res);
}

/**
 * @brief LensItf::ResyncRequestIris
 *****************************************************************************/
void LensItf::ResyncRequestIris()
{
    GetLensInvert();
    GetLensIrisFine();
    GetLensIrisTable();
    GetLensIrisPosition();
    GetLensIrisSettings();
}

/**
 * @brief LensItf::ResyncFocus
 *****************************************************************************/
void LensItf::ResyncFocus()
{
    GetLensFocusPosition();
}

/**
 * @brief LensItf::ResyncFocusMotor
 *****************************************************************************/
void LensItf::ResyncFocusMotor()
{
    GetLensFocusMotorPosition();
}

/**
 * @brief LensItf::ResyncZoomPosition
 *****************************************************************************/
void LensItf::ResyncZoom()
{
    GetLensZoomPosition();
}

/**
 * @brief LensIft::isFineIrisSupported()
 */
bool LensItf::isFineIrisSupported()
{
    uint8_t value = 0;

    // get lens settings from device
    auto res = ctrl_protocol_get_lens_fine_iris(GET_PROTOCOL_INSTANCE(this),
                                                GET_CHANNEL_INSTANCE(this), &value);
    return (res == 0);
}

QString LensItf::getB4Info()
{
    QString ret;
    std::array<uint8_t, 80> info = { 0 };

    auto res = ctrl_protocol_get_lens_b4_info(GET_PROTOCOL_INSTANCE(this),
                                              GET_CHANNEL_INSTANCE(this), 1, info.data());
    if (res == 0) {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
        ret = QString(reinterpret_cast<char *>(info.data()));
    }

    return ret;
}
