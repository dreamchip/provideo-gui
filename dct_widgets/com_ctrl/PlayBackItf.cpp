/*
 * Copyright (C) 2017 Dream Chip Technologies GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
/**
 * @file    PlayBackItf.cpp
 *
 * @brief   Implementation of Playback Interface
 *
 *****************************************************************************/
#include <cerrno>
#include <cstring>

#include <QtDebug>

#include <ctrl_protocol/ctrl_protocol_playback.h>

#include "common.h"
#include "PlayBackItf.h"

/*
 * PlayBackItf::resync()
 *****************************************************************************/
void PlayBackItf::resync()
{
    // Get buffer count, so that GUI displays the correct amount of buffer entries
    GetCount();

    // Get status of all buffers (will emit a statusChanged event for each buffer)
    GetStatus();

    // Get record and play information
    GetRec();
    GetPlay();

    // Get record, playback and stop modes
    GetRecMode();
    GetPlayMode();
    GetStopMode();

    // Get current position in playback buffer
    GetPos();

    // Get mark in and mark out positions of current playback buffer
    GetMarkPos();
}

/*
 * PlayBackItf::GetRec
 *****************************************************************************/
void PlayBackItf::GetRec()
{
    // Is there a signal listener
    if (receivers(SIGNAL(RecChanged(int))) > 0) {
        uint8_t buffer_id = 0u;

        // get current recording buffer id
        int res = ctrl_protocol_get_rec(GET_PROTOCOL_INSTANCE(this), GET_CHANNEL_INSTANCE(this),
                                        &buffer_id);
        HANDLE_ERROR(res);

        // emit a RecChanged signal
        emit RecChanged(static_cast<int>(buffer_id));
    }
}

/*
 * PlayBackItf::GetRecMode
 *****************************************************************************/
void PlayBackItf::GetRecMode()
{
    // Is there a signal listener
    if (receivers(SIGNAL(RecModeChanged(int))) > 0) {
        uint8_t mode = 0u;

        // get stop mode from device
        int res = ctrl_protocol_get_rec_mode(GET_PROTOCOL_INSTANCE(this),
                                             GET_CHANNEL_INSTANCE(this), &mode);
        HANDLE_ERROR(res);

        // emit a RecModeChanged signal
        emit RecModeChanged(static_cast<int>(mode));
    }
}

/*
 * PlayBackItf::GetPlay
 *****************************************************************************/
void PlayBackItf::GetPlay()
{
    // Is there a signal listener
    if (receivers(SIGNAL(PlayChanged(int, int))) > 0) {
        int16_t values[2];

        // get current playback buffer id and speed from device
        int res = ctrl_protocol_get_play(GET_PROTOCOL_INSTANCE(this), GET_CHANNEL_INSTANCE(this), 2,
                                         values);
        HANDLE_ERROR(res);

        // emit a PlayChanged signal
        emit PlayChanged(static_cast<int>(values[0]), static_cast<int>(values[1]));
    }
}

/*
 * PlayBackItf::GetPlayMode
 *****************************************************************************/
void PlayBackItf::GetPlayMode()
{
    // Is there a signal listener
    if (receivers(SIGNAL(PlayModeChanged(int))) > 0) {
        uint8_t mode = 0u;

        // get stop mode from device
        int res = ctrl_protocol_get_play_mode(GET_PROTOCOL_INSTANCE(this),
                                              GET_CHANNEL_INSTANCE(this), &mode);
        HANDLE_ERROR(res);

        // emit a PlayModeChanged signal
        emit PlayModeChanged(static_cast<int>(mode));
    }
}

/*
 * PlayBackItf::GetStopMode
 *****************************************************************************/
void PlayBackItf::GetStopMode()
{
    // Is there a signal listener
    if (receivers(SIGNAL(StopModeChanged(int))) > 0) {
        uint8_t mode = 0u;

        // get stop mode from device
        int res = ctrl_protocol_get_stop_mode(GET_PROTOCOL_INSTANCE(this),
                                              GET_CHANNEL_INSTANCE(this), &mode);
        HANDLE_ERROR(res);

        // emit a StopModeChanged signal
        emit StopModeChanged(static_cast<int>(mode));
    }
}

/*
 * PlayBackItf::GetPos
 *****************************************************************************/
void PlayBackItf::GetPos()
{
    // Is there a signal listener
    if (receivers(SIGNAL(PosChanged(int))) > 0) {
        uint32_t pos = 0u;

        // get current buffer position from device
        int res = ctrl_protocol_get_pos(GET_PROTOCOL_INSTANCE(this), GET_CHANNEL_INSTANCE(this),
                                        &pos);
        HANDLE_ERROR(res);

        // emit a PosChanged signal
        emit PosChanged(static_cast<int>(pos));
    }
}

/*
 * PlayBackItf::GetMarkPos
 *****************************************************************************/
void PlayBackItf::GetMarkPos()
{
    // Is there a signal listener
    if (receivers(SIGNAL(MarkPosChanged(int, int))) > 0) {
        uint32_t mark_pos[2];

        // get mark in and mark out position from device
        int res = ctrl_protocol_get_mark_pos(GET_PROTOCOL_INSTANCE(this),
                                             GET_CHANNEL_INSTANCE(this), 2, mark_pos);
        HANDLE_ERROR(res);

        // emit a MarkPosChanged signal
        emit MarkPosChanged(static_cast<int>(mark_pos[0]), static_cast<int>(mark_pos[1]));
    }
}

/*
 * PlayBackItf::GetStatus
 *****************************************************************************/
void PlayBackItf::GetStatus()
{
    // Is there a signal listener
    if (receivers(SIGNAL(StatusChanged(int, int, int, QString))) > 0) {
        // get number of recording buffers from device
        uint8_t num = 0u;
        int res = ctrl_protocol_get_count(GET_PROTOCOL_INSTANCE(this), GET_CHANNEL_INSTANCE(this),
                                          &num);
        HANDLE_ERROR(res);

        // Loop over all buffers, get their status and post it
        for (uint8_t i = 1; i <= num; i++) {
            ctrl_protocol_buffer_info_t status;
            status.buffer_id = i;

            // get mark in and mark out position from device
            int res = ctrl_protocol_get_status(GET_PROTOCOL_INSTANCE(this),
                                               GET_CHANNEL_INSTANCE(this), sizeof(status),
                                               reinterpret_cast<uint8_t *>(&status));
            HANDLE_ERROR(res);

            // emit a StatusChanged signal
            emit StatusChanged(static_cast<int>(status.buffer_id),
                               static_cast<int>(status.num_frames),
                               static_cast<int>(status.max_frames), QString(status.status));
        }
    }
}

/*
 * PlayBackItf::GetCount
 *****************************************************************************/
void PlayBackItf::GetCount()
{
    // Is there a signal listener
    if (receivers(SIGNAL(CountChanged(int))) > 0) {
        uint8_t num = 0u;

        // get number of recording buffers from device
        int res = ctrl_protocol_get_count(GET_PROTOCOL_INSTANCE(this), GET_CHANNEL_INSTANCE(this),
                                          &num);
        HANDLE_ERROR(res);

        // emit a CountChanged signal
        emit CountChanged(static_cast<int>(num));
    }
}

/*
 * PlayBackItf::GetStatus
 *****************************************************************************/
void PlayBackItf::GetHealth()
{
    // Is there a signal listener
    if (receivers(SIGNAL(HealthChanged(int, int, QString, int, int, int, int))) > 0) {
        ctrl_protocol_ssd_status_t status[2];

        // get mark in and mark out position from device
        int res = ctrl_protocol_get_health(GET_PROTOCOL_INSTANCE(this), GET_CHANNEL_INSTANCE(this),
                                           sizeof(status), reinterpret_cast<uint8_t *>(status));
        HANDLE_ERROR(res);

        // emit a HealthChanged signal for each SSD
        for (int i = 0; i < 2; i++) {
            emit HealthChanged(
                    static_cast<int>(status[i].ssd_id), static_cast<int>(status[i].status_code),
                    QString(status[i].status_string), static_cast<int>(status[i].temp_min),
                    static_cast<int>(status[i].temp), static_cast<int>(status[i].temp_max),
                    static_cast<int>(status[i].lifetime));
        }
    }
}

/*
 * PlayBackItf::onRecChange
 *****************************************************************************/
void PlayBackItf::onRecChange(int buffer_id)
{
    // set recording buffer on device
    int res = ctrl_protocol_set_rec(GET_PROTOCOL_INSTANCE(this), GET_CHANNEL_INSTANCE(this),
                                    static_cast<uint8_t>(buffer_id));
    HANDLE_ERROR(res);

    // update status and record status to refresh the GUI
    GetStatus();
    GetRec();
}

/*
 * PlayBackItf::onRecModeChange
 *****************************************************************************/
void PlayBackItf::onRecModeChange(int mode)
{
    // set recording mode on device
    int res = ctrl_protocol_set_rec_mode(GET_PROTOCOL_INSTANCE(this), GET_CHANNEL_INSTANCE(this),
                                         static_cast<uint8_t>(mode));
    HANDLE_ERROR(res);
}

/*
 * PlayBackItf::onRecStop
 *****************************************************************************/
void PlayBackItf::onRecStop()
{
    // stop recording
    int res = ctrl_protocol_rec_stop(GET_PROTOCOL_INSTANCE(this), GET_CHANNEL_INSTANCE(this));
    HANDLE_ERROR(res);
}

/*
 * PlayBackItf::onPlayChange
 *****************************************************************************/
void PlayBackItf::onPlayChange(int buffer_id, int speed)
{
    // start playback from buffer
    int16_t play_config[2];
    play_config[0] = static_cast<int16_t>(buffer_id);
    play_config[1] = static_cast<int16_t>(speed);

    int res = ctrl_protocol_set_play(GET_PROTOCOL_INSTANCE(this), GET_CHANNEL_INSTANCE(this), 2,
                                     play_config);
    HANDLE_ERROR(res);

    // update status, playback status and mark positions to refresh the GUI
    GetStatus();
    GetPlay();
    GetMarkPos();
}

/*
 * PlayBackItf::onPlayModeChange
 *****************************************************************************/
void PlayBackItf::onPlayModeChange(int mode)
{
    // set playback mode on device
    int res = ctrl_protocol_set_play_mode(GET_PROTOCOL_INSTANCE(this), GET_CHANNEL_INSTANCE(this),
                                          static_cast<uint8_t>(mode));
    HANDLE_ERROR(res);
}

/*
 * PlayBackItf::onPause
 *****************************************************************************/
void PlayBackItf::onPause()
{
    // pause playback
    int res = ctrl_protocol_pause(GET_PROTOCOL_INSTANCE(this), GET_CHANNEL_INSTANCE(this));
    HANDLE_ERROR(res);

    // update status and playback status to refresh the GUI
    GetStatus();
    GetPlay();
}

/*
 * PlayBackItf::onStop
 *****************************************************************************/
void PlayBackItf::onStop()
{
    // stop playback
    int res = ctrl_protocol_stop(GET_PROTOCOL_INSTANCE(this), GET_CHANNEL_INSTANCE(this));
    HANDLE_ERROR(res);

    // update status and playback status to refresh the GUI
    GetStatus();
    GetPlay();
}

/*
 * PlayBackItf::onStopModeChange
 *****************************************************************************/
void PlayBackItf::onStopModeChange(int mode)
{
    // set stop mode on device
    int res = ctrl_protocol_set_stop_mode(GET_PROTOCOL_INSTANCE(this), GET_CHANNEL_INSTANCE(this),
                                          static_cast<uint8_t>(mode));
    HANDLE_ERROR(res);
}

/*
 * PlayBackItf::onSeekChange
 *****************************************************************************/
void PlayBackItf::onSeekChange(int mode, int pos)
{
    // start playback from buffer
    int32_t seek_config[2];
    seek_config[0] = static_cast<int32_t>(mode);
    seek_config[1] = static_cast<int32_t>(pos);

    int res = ctrl_protocol_set_seek(GET_PROTOCOL_INSTANCE(this), GET_CHANNEL_INSTANCE(this), 2,
                                     seek_config);
    HANDLE_ERROR(res);

    // update position
    GetPos();
}

/*
 * PlayBackItf::onMarkIn
 *****************************************************************************/
void PlayBackItf::onMarkIn(int pos)
{
    // set mark in position
    int res = ctrl_protocol_mark_in(GET_PROTOCOL_INSTANCE(this), GET_CHANNEL_INSTANCE(this),
                                    static_cast<uint32_t>(pos));
    HANDLE_ERROR(res);

    // Update mark positions
    GetMarkPos();
}

/*
 * PlayBackItf::onMarkOut
 *****************************************************************************/
void PlayBackItf::onMarkOut(int pos)
{
    // set mark out position
    int res = ctrl_protocol_mark_out(GET_PROTOCOL_INSTANCE(this), GET_CHANNEL_INSTANCE(this),
                                     static_cast<uint32_t>(pos));
    HANDLE_ERROR(res);

    // Update mark positions
    GetMarkPos();
}

/*
 * PlayBackItf::onFreeChange
 *****************************************************************************/
void PlayBackItf::onFreeChange(int buffer_id)
{
    // set recording buffer on device
    int res = ctrl_protocol_set_free(GET_PROTOCOL_INSTANCE(this), GET_CHANNEL_INSTANCE(this),
                                     static_cast<uint8_t>(buffer_id));
    HANDLE_ERROR(res);

    // get new buffer count and status to show the changes in the GUI
    GetCount();
    GetStatus();
    GetRec();
    GetPlay();
}

/*
 * PlayBackItf::onCountChange
 *****************************************************************************/
void PlayBackItf::onCountChange(int num)
{
    // set amount of recording buffers
    int res = ctrl_protocol_set_count(GET_PROTOCOL_INSTANCE(this), GET_CHANNEL_INSTANCE(this),
                                      static_cast<uint8_t>(num));
    HANDLE_ERROR(res);

    // get new buffer count and status to show the changes in the GUI
    GetCount();
    GetStatus();
    GetRec();
    GetPlay();
}

/*
 * PlayBackItf::onCountChange
 *****************************************************************************/
void PlayBackItf::onUpdateStatus()
{
    GetStatus();
}

/*
 * PlayBackItf::onCountChange
 *****************************************************************************/
void PlayBackItf::onUpdatePos()
{
    GetPos();
}

/*
 * PlayBackItf::onUpdateHealth
 *****************************************************************************/
void PlayBackItf::onUpdateHealth()
{
    GetHealth();
}
