/*
 * Copyright (C) 2022 Dream Chip Technologies GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
/**
 * @file    zfcmotoroffsetpage.h
 * @brief   Zoom Focus Calibration (Wizard) Motor Offset Page
 *****************************************************************************/
#ifndef ZFCMOTOROFFSETPAGE_H
#define ZFCMOTOROFFSETPAGE_H

#include <QWizardPage>

namespace Ui {
class ZfcMotorOffsetPage;
}

class ZfcMotorOffsetPage : public QWizardPage
{
    Q_OBJECT

public:
    explicit ZfcMotorOffsetPage(QWidget *parent = nullptr);
    ~ZfcMotorOffsetPage() override;

    void enableFinish(bool enable);

    void cleanupPage() override;

signals:
    void LensMotorOffsetChanged(int value);

public slots:
    void onLensMotorOffsetChange(int value);

public:
    Ui::ZfcMotorOffsetPage *ui; // is public to control it from the wizard
};

#endif // ZFCMOTOROFFSETPAGE_H
