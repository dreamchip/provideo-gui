/*
 * Copyright (C) 2022 Dream Chip Technologies GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
/**
 * @file    zfcmotoroffsetpage.cpp
 * @brief   Zoom Focus Calibration (Wizard) Motor Offset Page
 *****************************************************************************/
#include "zfcmotoroffsetpage.h"
#include "ui_zfcmotoroffsetpage.h"

ZfcMotorOffsetPage::ZfcMotorOffsetPage(QWidget *parent)
    : QWizardPage(parent), ui(new Ui::ZfcMotorOffsetPage)
{
    ui->setupUi(this);

    registerField("finish", ui->cbxFinish);

    connect(ui->sbxMotorOffset, QOverload<int>::of(&QSpinBox::valueChanged), this,
            [=](int value) { emit LensMotorOffsetChanged(value); });
    connect(ui->sldMotorOffset, QOverload<int>::of(&QSlider::valueChanged), this,
            [=](int value) { emit LensMotorOffsetChanged(value); });
}

ZfcMotorOffsetPage::~ZfcMotorOffsetPage()
{
    delete ui;
}

void ZfcMotorOffsetPage::enableFinish(bool enable)
{
    ui->cbxFinish->setEnabled(enable);
}

void ZfcMotorOffsetPage::cleanupPage()
{
    enableFinish(true);
}

void ZfcMotorOffsetPage::onLensMotorOffsetChange(int value)
{
    ui->sbxMotorOffset->setValue(value);
    ui->sldMotorOffset->setValue(value);
}
