/*
 * Copyright (C) 2022 Dream Chip Technologies GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
/**
 * @file    zfcfocuspospage.cpp
 * @brief   Zoom Focus Calibration (Wizard) Focus Position Page
 *****************************************************************************/
#include "zfcfocuspospage.h"
#include "ui_zfcfocuspospage.h"

ZfcFocusPosPage::ZfcFocusPosPage(QWidget *parent) : QWizardPage(parent), ui(new Ui::ZfcFocusPosPage)
{
    ui->setupUi(this);

    connect(ui->sbxFocusPos, QOverload<int>::of(&QSpinBox::valueChanged), this, [=](int value) {
        if (isVisible()) {
            emit LensFocusMotorPositionChanged(value);
        }
    });
    connect(ui->sldFocusPos, QOverload<int>::of(&QSlider::valueChanged), this, [=](int value) {
        if (isVisible()) {
            emit LensFocusMotorPositionChanged(value);
        }
    });
}

ZfcFocusPosPage::~ZfcFocusPosPage()
{
    delete ui;
}

int ZfcFocusPosPage::getFocusMotorPos() const
{
    return ui->sldFocusPos->value();
}

void ZfcFocusPosPage::onLensFocusMotorPositionChange(int value)
{
    ui->sldFocusPos->setValue(value);
    ui->sbxFocusPos->setValue(value);
}
