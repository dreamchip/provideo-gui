/*
 * Copyright (C) 2017 Dream Chip Technologies GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
/**
 * @file    fltbox.cpp
 *
 * @brief   Implementation of filter configuration box
 *
 *****************************************************************************/
#include "defines.h"
#include "playbackbox.h"
#include "ui_playbackbox.h"

#include <QtDebug>
#include <QSignalMapper>
#include <QTimer>
#include <QMessageBox>

/*
 * namespaces
 *****************************************************************************/
namespace Ui {
class UI_PlayBackBox;
}

/*
 * Local defines
 *****************************************************************************/
#define UPDATE_INTERVAL_MS (250)

/*
 * Settings
 *****************************************************************************/
#define PLAYBACK_SETTINGS_SECTION_NAME ("PLAYBACK")
#define PLAYBACK_SETTINGS_REC_MODE ("rec_mode")
#define PLAYBACK_SETTINGS_PLAY_MODE ("play_mode")
#define PLAYBACK_SETTINGS_STOP_MODE ("stop_mode")

/*
 * PlayBackBox::PrivateData
 *****************************************************************************/
class PlayBackBox::PrivateData
{
public:
    PrivateData()
        : m_ui(new Ui::UI_PlayBackBox),
          m_updateTimer(),
          m_bufferCount(0),
          m_selectedBufferID(0),
          m_numFree(0),
          m_speed(10),
          m_playbackID(0),
          m_playbackPaused(false)
    {
        // do nothing
    }

    ~PrivateData() { delete m_ui; }

    Ui::UI_PlayBackBox *m_ui; /**< ui handle */
    QTimer m_updateTimer; /**< update buffer status timer */
    int m_bufferCount; /**< amount of buffers */
    int m_selectedBufferID; /**< ID of the currently selected buffer or 0 if no buffer selected */
    int m_numFree; /**< Amount of buffers that are reported as being free */
    int m_speed; /**< playback speed */
    int m_playbackID; /**< ID of the buffer which is currently being played or 0 if no playback
                         running */
    bool m_playbackPaused; /**< If true, playback is currently paused. */
};

/*
 * PlayBackBox::PlayBackBox
 *****************************************************************************/
PlayBackBox::PlayBackBox(QWidget *parent) : DctWidgetBox(parent), d_data(new PrivateData)
{
    // initialize UI
    d_data->m_ui->setupUi(this);

    // setup UI elements
    // stretch table to fill complete area
    d_data->m_ui->tblBufferStatus->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    d_data->m_ui->tblBufferStatus->setSelectionMode(QAbstractItemView::SingleSelection);
    d_data->m_ui->tblBufferStatus->setSelectionBehavior(QAbstractItemView::SelectRows);

    // disable the apply buffer count button, it will be enabled as soon as a new amount of buffers
    // is selected
    d_data->m_ui->btnApplyBufferCount->setEnabled(false);

    // disable the playback controls, they will be enabled if playback is running
    d_data->m_ui->frmPlaybackControl->setEnabled(false);

    // setup range sliders
    d_data->m_ui->sldPlaybackPosition->SetOptions(RangeSlider::RightHandle);
    d_data->m_ui->sldPlaybackPosition->SetRange(1, 2);
    d_data->m_ui->sldPlaybackPosition->SetUpperValue(0);
    d_data->m_ui->sldMarkedArea->SetRange(1, 2);
    d_data->m_ui->sldMarkedArea->SetOptions(RangeSlider::DoubleHandles);

    // setup timer to update the temperature / fan data every second
    /* The timer will be started with every show event and stopped when a
     * hide event occurs. This ensures it is only running when it is needed. */
    connect(&d_data->m_updateTimer, SIGNAL(timeout()), this, SLOT(onUpdateTimerExpired()));

    // Connect handlers for UI interaction
    connect(d_data->m_ui->tblBufferStatus, SIGNAL(itemSelectionChanged()), this,
            SLOT(onBufferSelectionChanged()));

    connect(d_data->m_ui->cbxRecordMode, SIGNAL(currentIndexChanged(int)), this,
            SLOT(onCbxRecordModeChanged(int)));
    connect(d_data->m_ui->cbxPlaybackMode, SIGNAL(currentIndexChanged(int)), this,
            SLOT(onCbxPlayModeChanged(int)));
    connect(d_data->m_ui->cbxStopMode, SIGNAL(currentIndexChanged(int)), this,
            SLOT(onCbxStopModeChanged(int)));

    connect(d_data->m_ui->sbxBufferCount, SIGNAL(valueChanged(int)), this,
            SLOT(onSbxBufferCountChanged(int)));
    connect(d_data->m_ui->btnApplyBufferCount, SIGNAL(clicked()), this,
            SLOT(onApplyBufferCountClicked()));

    connect(d_data->m_ui->btnFreeSelectedBuffer, SIGNAL(clicked()), this,
            SLOT(onFreeSelectedBufferClicked()));
    connect(d_data->m_ui->btnFreeAllBuffers, SIGNAL(clicked()), this,
            SLOT(onFreeAllBuffersClicked()));

    connect(d_data->m_ui->btnStartRecording, SIGNAL(clicked()), this,
            SLOT(onStartRecordingClicked()));
    connect(d_data->m_ui->btnStopRecording, SIGNAL(clicked()), this,
            SLOT(onStopRecordingClicked()));
    connect(d_data->m_ui->btnStartPlayback, SIGNAL(clicked()), this,
            SLOT(onStartPlaybackClicked()));
    connect(d_data->m_ui->btnStopPlayback, SIGNAL(clicked()), this, SLOT(onStopPlaybackClicked()));

    connect(d_data->m_ui->sldPlaybackPosition, SIGNAL(upperValueChanged(int)), this,
            SLOT(onPositionSliderMoved(int)));

    // Use lambda expression to have only one function handle all "skip clicked" buttons
    connect(d_data->m_ui->btnSkipLeft1, &QPushButton::clicked, this,
            [this]() { doRelativeSeek(-1); });
    connect(d_data->m_ui->btnSkipLeft5, &QPushButton::clicked, this,
            [this]() { doRelativeSeek(-5); });
    connect(d_data->m_ui->btnSkipLeft10, &QPushButton::clicked, this,
            [this]() { doRelativeSeek(-10); });
    connect(d_data->m_ui->btnSkipLeft50, &QPushButton::clicked, this,
            [this]() { doRelativeSeek(-50); });
    connect(d_data->m_ui->btnSkipLeft100, &QPushButton::clicked, this,
            [this]() { doRelativeSeek(-100); });
    connect(d_data->m_ui->btnSkipRight1, &QPushButton::clicked, this,
            [this]() { doRelativeSeek(1); });
    connect(d_data->m_ui->btnSkipRight5, &QPushButton::clicked, this,
            [this]() { doRelativeSeek(5); });
    connect(d_data->m_ui->btnSkipRight10, &QPushButton::clicked, this,
            [this]() { doRelativeSeek(10); });
    connect(d_data->m_ui->btnSkipRight50, &QPushButton::clicked, this,
            [this]() { doRelativeSeek(50); });
    connect(d_data->m_ui->btnSkipRight100, &QPushButton::clicked, this,
            [this]() { doRelativeSeek(100); });

    connect(d_data->m_ui->sldMarkedArea, SIGNAL(lowerValueChanged(int)), this,
            SLOT(onMarkInPositionSliderMoved(int)));
    connect(d_data->m_ui->sldMarkedArea, SIGNAL(upperValueChanged(int)), this,
            SLOT(onMarkOutPositionSliderMoved(int)));
    connect(d_data->m_ui->btnMarkIn, SIGNAL(clicked()), this, SLOT(onMarkInClicked()));
    connect(d_data->m_ui->btnMarkOut, SIGNAL(clicked()), this, SLOT(onMarkOutClicked()));
    connect(d_data->m_ui->btnResetMarks, SIGNAL(clicked()), this, SLOT(onResetMarksClicked()));

    connect(d_data->m_ui->sldPlaybackSpeed, SIGNAL(valueChanged(int)), this,
            SLOT(onPlaybackSpeedSliderMoved(int)));
    connect(d_data->m_ui->btnPlay, SIGNAL(clicked()), this, SLOT(onPlayClicked()));

    // Use lambda expression to have only one function handle all "play forward/backward clicked"
    // buttons
    connect(d_data->m_ui->btnPlayForwardHalf, &QPushButton::clicked, this,
            [this]() { changePlaybackSpeed(5); });
    connect(d_data->m_ui->btnPlayForward1, &QPushButton::clicked, this,
            [this]() { changePlaybackSpeed(10); });
    connect(d_data->m_ui->btnPlayForward2, &QPushButton::clicked, this,
            [this]() { changePlaybackSpeed(20); });
    connect(d_data->m_ui->btnPlayForward5, &QPushButton::clicked, this,
            [this]() { changePlaybackSpeed(50); });
    connect(d_data->m_ui->btnPlayForward10, &QPushButton::clicked, this,
            [this]() { changePlaybackSpeed(100); });
    connect(d_data->m_ui->btnPlayBackwardHalf, &QPushButton::clicked, this,
            [this]() { changePlaybackSpeed(-5); });
    ;
    connect(d_data->m_ui->btnPlayBackward1, &QPushButton::clicked, this,
            [this]() { changePlaybackSpeed(-10); });
    connect(d_data->m_ui->btnPlayBackward2, &QPushButton::clicked, this,
            [this]() { changePlaybackSpeed(-20); });
    connect(d_data->m_ui->btnPlayBackward5, &QPushButton::clicked, this,
            [this]() { changePlaybackSpeed(-50); });
    connect(d_data->m_ui->btnPlayBackward10, &QPushButton::clicked, this,
            [this]() { changePlaybackSpeed(-100); });
}

/*
 * PlayBackBox::~PlayBackBox
 *****************************************************************************/
PlayBackBox::~PlayBackBox()
{
    delete d_data;
}

/*
 * PlayBackBox::showEvent
 *****************************************************************************/
void PlayBackBox::showEvent(QShowEvent *event)
{
    // Call inherited function
    DctWidgetBox::showEvent(event);

    // Update temperature and fan readouts
    onUpdateTimerExpired();

    // Start the update timer
    d_data->m_updateTimer.start(UPDATE_INTERVAL_MS);
}

/*
 * PlayBackBox::hideEvent
 *****************************************************************************/
void PlayBackBox::hideEvent(QHideEvent *event)
{
    // Call inherited function
    DctWidgetBox::hideEvent(event);

    // Start the update timer
    d_data->m_updateTimer.stop();
}

/*
 * PlayBackBox::prepareMode
 *****************************************************************************/
void PlayBackBox::prepareMode(const Mode)
{
    // do nothing here
}

/*
 * PlayBackBox::loadSettings
 *****************************************************************************/
void PlayBackBox::loadSettings(QSettings &s)
{
    // load from file
    s.beginGroup(PLAYBACK_SETTINGS_SECTION_NAME);
    setRecordMode(s.value(PLAYBACK_SETTINGS_REC_MODE).toBool());
    setPlayMode(s.value(PLAYBACK_SETTINGS_PLAY_MODE).toInt());
    setStopMode(s.value(PLAYBACK_SETTINGS_STOP_MODE).toInt());
    s.endGroup();
}

/*
 * PlayBackBox::saveSettings
 *****************************************************************************/
void PlayBackBox::saveSettings(QSettings &s)
{
    // Write to file
    s.beginGroup(PLAYBACK_SETTINGS_SECTION_NAME);
    s.setValue(PLAYBACK_SETTINGS_REC_MODE, getRecordMode());
    s.setValue(PLAYBACK_SETTINGS_PLAY_MODE, getPlayMode());
    s.setValue(PLAYBACK_SETTINGS_STOP_MODE, getStopMode());
    s.endGroup();
}

/*
 * PlayBackBox::applySettings
 *****************************************************************************/
void PlayBackBox::applySettings(void)
{
    emit RecordModeChanged(getRecordMode());
    emit PlayModeChanged(getPlayMode());
    emit StopModeChanged(getStopMode());
}

/*
 * PlayBackBox::beforeCommand
 *****************************************************************************/
void PlayBackBox::beforeCommand()
{
    // Stop the update timer
    d_data->m_updateTimer.stop();

    // Show wait cursor
    setWaitCursor();
}

/*
 * PlayBackBox::afterCommand
 *****************************************************************************/
void PlayBackBox::afterCommand()
{
    // Show normal curosr
    setNormalCursor();

    // Start the update timer
    d_data->m_updateTimer.start(UPDATE_INTERVAL_MS);
}

/*
 * PlayBackBox::getRecordMode
 *****************************************************************************/
int PlayBackBox::getRecordMode()
{
    return d_data->m_ui->cbxRecordMode->currentIndex();
}

/*
 * PlayBackBox::setRecordMode
 *****************************************************************************/
void PlayBackBox::setRecordMode(int mode)
{
    d_data->m_ui->cbxRecordMode->blockSignals(true);
    d_data->m_ui->cbxRecordMode->setCurrentIndex(mode);
    d_data->m_ui->cbxRecordMode->blockSignals(false);

    emit RecordModeChanged(mode);
}

/*
 * PlayBackBox::getPlayMode
 *****************************************************************************/
int PlayBackBox::getPlayMode()
{
    return d_data->m_ui->cbxPlaybackMode->currentIndex();
}

/*
 * PlayBackBox::setPlayMode
 *****************************************************************************/
void PlayBackBox::setPlayMode(int mode)
{
    d_data->m_ui->cbxPlaybackMode->blockSignals(true);
    d_data->m_ui->cbxPlaybackMode->setCurrentIndex(mode);
    d_data->m_ui->cbxPlaybackMode->blockSignals(false);

    emit PlayModeChanged(mode);
}

/*
 * PlayBackBox::getStopMode
 *****************************************************************************/
int PlayBackBox::getStopMode()
{
    return d_data->m_ui->cbxStopMode->currentIndex();
}

/*
 * PlayBackBox::setStopMode
 *****************************************************************************/
void PlayBackBox::setStopMode(int mode)
{
    d_data->m_ui->cbxStopMode->blockSignals(true);
    d_data->m_ui->cbxStopMode->setCurrentIndex(mode);
    d_data->m_ui->cbxStopMode->blockSignals(false);

    emit StopModeChanged(mode);
}

/*
 * PlayBackBox::updatePlayButton
 *****************************************************************************/
void PlayBackBox::updatePlayButton(bool playing)
{
    // If playback is paused, button has normal description
    if (!playing) {
        d_data->m_playbackPaused = true;
        d_data->m_ui->btnPlay->setText("Play");
        d_data->m_ui->btnPlay->setIcon(QIcon(":/icons/play_icon_white.ico"));
    }
    // Else if playback is running, show pause function
    else {
        d_data->m_playbackPaused = false;
        d_data->m_ui->btnPlay->setText("Pause");
        d_data->m_ui->btnPlay->setIcon(QIcon(":/icons/pause_icon_white.ico"));
    }
}

/*
 * PlayBackBox::doRelativeSeek
 *****************************************************************************/
void PlayBackBox::doRelativeSeek(int frames)
{
    // do a relative seek
    beforeCommand();
    emit SeekChanged(0, frames);
    afterCommand();
}

/*
 * PlayBackBox::changePlaybackSpeed
 *****************************************************************************/
void PlayBackBox::changePlaybackSpeed(int speed)
{
    d_data->m_speed = speed;

    // change playback speed
    beforeCommand();
    emit PlayChanged(d_data->m_playbackID, d_data->m_speed);
    afterCommand();
}

/*
 * PlayBackBox::onBufferCountChange
 *****************************************************************************/
void PlayBackBox::onBufferCountChange(int num)
{
    // Update buffer count spinbox and local buffer count variable
    d_data->m_ui->sbxBufferCount->blockSignals(true);
    d_data->m_ui->sbxBufferCount->setValue(num);
    d_data->m_ui->sbxBufferCount->blockSignals(false);
    d_data->m_bufferCount = num;

    // Set amount of table rows
    d_data->m_ui->tblBufferStatus->clearContents();
    d_data->m_ui->tblBufferStatus->setRowCount(num);
}

/*
 * PlayBackBox::onBufferStatusChange
 *****************************************************************************/
void PlayBackBox::onBufferStatusChange(int buffer_id, int num_frames, int max_frames,
                                       QString status)
{
    /* Note: This function will be called for each buffer, the calls will be done
     * in order starting with buffer_id 1 and ending with buffer_id == buffer_count.
     * The buffer count will be known since onBufferCountChange will be called by
     * the PlayBackItf before calling onBufferStatusChange. */

    if (buffer_id > d_data->m_ui->tblBufferStatus->rowCount()) {
        qDebug() << "The buffer_id: " << buffer_id << " is bigger than the row count of the table: "
                 << d_data->m_ui->tblBufferStatus->rowCount();
        return;
    }

    // If this is the first buffer, clear local variables
    static int numFree = 0;
    static int playbackID = 0;
    static bool playbackPaused = false;
    static bool recordRunning = false;
    if (buffer_id == 1) {
        numFree = 0;
        playbackID = 0;
        playbackPaused = false;
        recordRunning = false;
    }

    // Show data in table row
    int row = buffer_id - 1;
    d_data->m_ui->tblBufferStatus->setItem(row, 0,
                                           new QTableWidgetItem(QString::number(buffer_id)));
    d_data->m_ui->tblBufferStatus->setItem(row, 1,
                                           new QTableWidgetItem(QString::number(num_frames)));
    d_data->m_ui->tblBufferStatus->setItem(row, 2,
                                           new QTableWidgetItem(QString::number(max_frames)));
    d_data->m_ui->tblBufferStatus->setItem(row, 3, new QTableWidgetItem(status));

    // Update UI elements to represent current record / playback state
    // Note: The satus can be either of free, used, record, play or pause
    if (status == "record") {
        // set red background of the status cell
        d_data->m_ui->tblBufferStatus->item(row, 3)->setBackground(QColorConstants::DarkRed);

        // remember that recording is running
        recordRunning = true;
    } else if (status == "play") {
        // set green background of the status cell
        d_data->m_ui->tblBufferStatus->item(row, 3)->setBackground(QColorConstants::DarkGreen);

        // update the seek bar and mark bar if their length changed
        if (d_data->m_ui->sldPlaybackPosition->GetMaximun() != num_frames) {
            d_data->m_ui->sldPlaybackPosition->blockSignals(true);
            d_data->m_ui->sldPlaybackPosition->SetMaximum(num_frames);
            d_data->m_ui->sldPlaybackPosition->blockSignals(false);
        }
        if (d_data->m_ui->sldMarkedArea->GetMaximun() != num_frames) {
            d_data->m_ui->sldMarkedArea->blockSignals(true);
            d_data->m_ui->sldMarkedArea->SetMaximum(num_frames);
            d_data->m_ui->sldMarkedArea->blockSignals(false);
        }

        // remember that playback is running
        playbackID = buffer_id;
        playbackPaused = false;
    } else if (status == "pause") {
        // set green background of the status cell
        d_data->m_ui->tblBufferStatus->item(row, 3)->setBackground(QColorConstants::DarkGreen);

        /* if we previously started a playback which was then paused, than
         * we still consider this to be our playback buffer */
        if (playbackID == 0 && d_data->m_playbackID == buffer_id) {
            playbackID = buffer_id;
            playbackPaused = true;
        }
    } else if (status == "used") {
        // set yellow background of the status cell
        d_data->m_ui->tblBufferStatus->item(row, 3)->setBackground(QColorConstants::DarkYellow);
    } else if (status == "free") {
        // set blue background of the status cell
        d_data->m_ui->tblBufferStatus->item(row, 3)->setBackground(QColorConstants::DarkBlue);

        // increment free buffer count
        numFree++;
    }

    // Check if this was the status of the last buffer
    if (buffer_id == d_data->m_bufferCount) {
        // store local variables in class variables
        d_data->m_numFree = numFree;
        d_data->m_playbackID = playbackID;
        d_data->m_ui->lblPlaybackBuffer->setText(QString("%1").arg(d_data->m_playbackID));

        /* if playback state got changed automatically (e.g. because we reached end of playback
         * buffer, or the play mode was changed), call onPlayChange to update UI elements */
        if (d_data->m_playbackPaused != playbackPaused) {
            onPlayChange(d_data->m_playbackID, playbackPaused ? 0 : d_data->m_speed);
        }

        // enable the free all buffers button if not all buffers are free
        d_data->m_ui->btnFreeAllBuffers->setEnabled(d_data->m_numFree < d_data->m_bufferCount);

        // call onBufferSelectionChanged to update UI elements
        onBufferSelectionChanged();

        // if playback is running, enable playback controls and the stop playback button
        d_data->m_ui->frmPlaybackControl->setEnabled(d_data->m_playbackID != 0);
        d_data->m_ui->btnStopPlayback->setEnabled(d_data->m_playbackID != 0);

        /* If record is running, disable record mode checkbox, as the record mode can not be
         * changed, while a recording is running. Also enable the stop record button. */
        d_data->m_ui->cbxRecordMode->setEnabled(!recordRunning);
        d_data->m_ui->btnStopRecording->setEnabled(recordRunning);
    }
}

/*
 * PlayBackBox::onRecordChange
 *****************************************************************************/
void PlayBackBox::onRecordChange(int buffer_id)
{
    (void)buffer_id;

    // FIXME: Nothing to do here
}

/*
 * PlayBackBox::onRecordModeChange
 *****************************************************************************/
void PlayBackBox::onRecordModeChange(int mode)
{
    d_data->m_ui->cbxRecordMode->blockSignals(true);
    d_data->m_ui->cbxRecordMode->setCurrentIndex(mode);
    d_data->m_ui->cbxRecordMode->blockSignals(false);
}

/*
 * PlayBackBox::onPlayChange
 *****************************************************************************/
void PlayBackBox::onPlayChange(int buffer_id, int speed)
{
    (void)buffer_id;

    if (speed != 0) {
        // Playback is active
        updatePlayButton(true);

        // store speed if it is not 0 (in pause state we want to preserve our internal speed value)
        d_data->m_speed = speed;
    } else {
        // If speed is 0, playback is paused
        updatePlayButton(false);
    }

    // Update speed slider with new value
    d_data->m_ui->sldPlaybackSpeed->blockSignals(true);
    d_data->m_ui->sldPlaybackSpeed->setValue(speed);
    d_data->m_ui->sldPlaybackSpeed->blockSignals(false);

    // Display speed in label
    if (d_data->m_playbackPaused) {
        // In pause state, show old speed which will be used when the user clicks play again
        d_data->m_ui->lblPlaybackSpeed->setText(
                QString("Playback Speed: x%1 (paused)").arg(d_data->m_speed / 10.0));
    } else {
        // Else show current speed
        d_data->m_ui->lblPlaybackSpeed->setText(QString("Playback Speed: x%1").arg(speed / 10.0));
    }
}

/*
 * PlayBackBox::onPlayModeChange
 *****************************************************************************/
void PlayBackBox::onPlayModeChange(int mode)
{
    d_data->m_ui->cbxPlaybackMode->blockSignals(true);
    d_data->m_ui->cbxPlaybackMode->setCurrentIndex(mode);
    d_data->m_ui->cbxPlaybackMode->blockSignals(false);
}

/*
 * PlayBackBox::onStopModeChange
 *****************************************************************************/
void PlayBackBox::onStopModeChange(int mode)
{
    d_data->m_ui->cbxStopMode->blockSignals(true);
    d_data->m_ui->cbxStopMode->setCurrentIndex(mode);
    d_data->m_ui->cbxStopMode->blockSignals(false);
}

/*
 * PlayBackBox::onPositionChange
 *****************************************************************************/
void PlayBackBox::onPositionChange(int pos)
{
    // If no playback is running pos will be 0
    if (pos == 0) {
        // Update position of the seek bar
        d_data->m_ui->sldPlaybackPosition->blockSignals(true);
        d_data->m_ui->sldPlaybackPosition->SetUpperValue(1);
        d_data->m_ui->sldPlaybackPosition->blockSignals(false);

        d_data->m_ui->lblFrameCount->setText(QString("Frame: ? / ?"));

        // Also show unknown mark-in and mark-out position
        d_data->m_ui->sldMarkedArea->blockSignals(true);
        d_data->m_ui->sldMarkedArea->SetLowerValue(d_data->m_ui->sldMarkedArea->GetMinimun());
        d_data->m_ui->sldMarkedArea->SetUpperValue(d_data->m_ui->sldMarkedArea->GetMaximun());
        d_data->m_ui->sldMarkedArea->blockSignals(false);

        d_data->m_ui->lblMarkIn->setText("Mark In: ?");
        d_data->m_ui->lblMarkOut->setText("Mark Out: ?");

        // And update playback speed text
        d_data->m_ui->lblPlaybackSpeed->setText("Playback Speed: x0 (stopped)");
    } else {
        // Update position of the seek bar
        d_data->m_ui->sldPlaybackPosition->blockSignals(true);
        d_data->m_ui->sldPlaybackPosition->SetUpperValue(pos);
        d_data->m_ui->sldPlaybackPosition->blockSignals(false);

        // Display position in label
        int max_pos = d_data->m_ui->sldPlaybackPosition->GetMaximun();
        d_data->m_ui->lblFrameCount->setText(QString("Frame: %1 / %2").arg(pos).arg(max_pos));
    }
}

/*
 * PlayBackBox::onMarkPositionChange
 *****************************************************************************/
void PlayBackBox::onMarkPositionChange(int mark_in, int mark_out)
{
    // Check if mark positions are valid, otherwise use defaults
    // Note: If no playback is running 0, 0 will be reported as mark positions
    if (mark_in == 0 && mark_out == 0) {
        mark_in = 1;
        mark_out = d_data->m_ui->sldMarkedArea->GetMaximun();
    }

    // Update positions of the mark in and out position on the mark bar
    d_data->m_ui->sldMarkedArea->blockSignals(true);
    d_data->m_ui->sldMarkedArea->SetLowerValue(mark_in);
    d_data->m_ui->sldMarkedArea->SetUpperValue(mark_out);
    d_data->m_ui->sldMarkedArea->blockSignals(false);

    // Display mark positions in labels
    d_data->m_ui->lblMarkIn->setText(QString("Mark In: %1").arg(mark_in));
    d_data->m_ui->lblMarkOut->setText(QString("Mark Out: %1").arg(mark_out));
}

/*
 * PlayBackBox::onUpdateTimerExpired
 *****************************************************************************/
void PlayBackBox::onUpdateTimerExpired()
{
    emit UpdateBufferStatus();
    emit UpdatePosition();
}

/*
 * PlayBackBox::onCbxRecordModeChanged
 *****************************************************************************/
void PlayBackBox::onBufferSelectionChanged()
{
    if (d_data->m_ui->tblBufferStatus->selectedItems().isEmpty()) {
        // No buffer selected
        d_data->m_selectedBufferID = 0;

        // Enable Start Record button if there is at least one free buffer
        d_data->m_ui->btnStartRecording->setEnabled(d_data->m_numFree > 0);

        // Disable start playback button
        d_data->m_ui->btnStartPlayback->setEnabled(false);

        // Disable free selected buffer button
        d_data->m_ui->btnFreeSelectedBuffer->setEnabled(false);
    } else {
        // Get ID of selected buffer
        d_data->m_selectedBufferID =
                d_data->m_ui->tblBufferStatus->selectedItems().first()->text().toInt();

        // Get status of selected buffer
        // Note: The satus can be either of free, used, record, play or pause
        QString status = d_data->m_ui->tblBufferStatus->selectedItems().last()->text();

        // Enable start record button if status is free
        d_data->m_ui->btnStartRecording->setEnabled(status == "free");

        // Enable start playback button if status is used
        d_data->m_ui->btnStartPlayback->setEnabled(status == "used" || status == "pause");

        // Enable the free selected buffer button if status is not free
        d_data->m_ui->btnFreeSelectedBuffer->setEnabled(status != "free");
    }
}

/*
 * PlayBackBox::onCbxRecordModeChanged
 *****************************************************************************/
void PlayBackBox::onCbxRecordModeChanged(int mode)
{
    // Emit signal
    beforeCommand();
    emit RecordModeChanged(mode);
    afterCommand();
}

/*
 * PlayBackBox::onCbxPlayModeChanged
 *****************************************************************************/
void PlayBackBox::onCbxPlayModeChanged(int mode)
{
    // Emit signal
    beforeCommand();
    emit PlayModeChanged(mode);
    afterCommand();
}

/*
 * PlayBackBox::onCbxStopModeChanged
 *****************************************************************************/
void PlayBackBox::onCbxStopModeChanged(int mode)
{
    // Emit signal
    beforeCommand();
    emit StopModeChanged(mode);
    afterCommand();
}

/*
 * PlayBackBox::onSbxBufferCountChanged
 *****************************************************************************/
void PlayBackBox::onSbxBufferCountChanged(int num)
{
    // If buffer count is different from the current setting, enable apply button
    d_data->m_ui->btnApplyBufferCount->setEnabled(num != d_data->m_bufferCount);
}

/*
 * PlayBackBox::onApplyBufferCountClicked
 *****************************************************************************/
void PlayBackBox::onApplyBufferCountClicked()
{
    // Ask user if he is really sure, as this will clear all buffers
    QMessageBox msgBox;
    msgBox.setWindowTitle("Change Buffer Count?");
    msgBox.setText("You are about to change the buffer count.");
    msgBox.setInformativeText("Changing the buffer count will reformat the internal storage. "
                              "This will invalidate all buffers. All recorded video data will "
                              "be lost.\n\n"
                              "Please also note that changing the buffer count will require a "
                              "restart of the image pipeline. If genlock is enabled this can "
                              "take up to 10 seconds.\n\n"
                              "Are you sure you want to change the buffer count?");
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    msgBox.setDefaultButton(QMessageBox::Yes);
    msgBox.setIcon(QMessageBox::Question);

    int ret = msgBox.exec();
    switch (ret) {
    case QMessageBox::Yes:
        // Disable the button, it will be enabled when the count gets changed in the spinbox
        d_data->m_ui->btnApplyBufferCount->setEnabled(false);

        // Emit signal
        beforeCommand();
        emit BufferCountChanged(d_data->m_ui->sbxBufferCount->value());
        afterCommand();

        // Clear selection of buffer table
        d_data->m_ui->tblBufferStatus->clearSelection();

        break;
    case QMessageBox::No:
        // Do nothing
        break;
    default:
        // should never be reached
        break;
    }
}

/*
 * PlayBackBox::onFreeSelectedBufferClicked
 *****************************************************************************/
void PlayBackBox::onFreeSelectedBufferClicked()
{
    /* Free selected buffer, then make sure it is still selected and set focus
     * to the start record button as this reflects the most common workflow. */
    beforeCommand();
    int selectedBuffer = d_data->m_selectedBufferID;
    emit FreeBuffer(selectedBuffer);
    d_data->m_ui->tblBufferStatus->selectRow(selectedBuffer - 1);
    d_data->m_ui->btnStartRecording->setFocus();
    afterCommand();
}

/*
 * PlayBackBox::onFreeAllBuffersClicked
 *****************************************************************************/
void PlayBackBox::onFreeAllBuffersClicked()
{
    // Emit signal
    /* Note: Freeing a buffer with ID 0 frees all buffers */
    beforeCommand();
    emit FreeBuffer(0);
    afterCommand();

    // Clear selection of buffer table
    d_data->m_ui->tblBufferStatus->clearSelection();
}

/*
 * PlayBackBox::onStartRecordingClicked
 *****************************************************************************/
void PlayBackBox::onStartRecordingClicked()
{
    // Emit signal
    /* Note: m_selectedBufferID will be 0 if no buffer is selected,
     * this will cause the device to automatically use the next free buffer. */
    beforeCommand();
    emit RecordChanged(d_data->m_selectedBufferID);
    afterCommand();

    // Clear selection of buffer table
    d_data->m_ui->tblBufferStatus->clearSelection();
}

/*
 * PlayBackBox::onStopRecordingClicked
 *****************************************************************************/
void PlayBackBox::onStopRecordingClicked()
{
    // Emit signal
    beforeCommand();
    emit RecordStopped();
    afterCommand();

    // Clear selection of buffer table
    d_data->m_ui->tblBufferStatus->clearSelection();
}

/*
 * PlayBackBox::onStartPlaybackClicked
 *****************************************************************************/
void PlayBackBox::onStartPlaybackClicked()
{
    // Set default speed (x1)
    d_data->m_speed = 10;

    /* Make sure to store playback buffer incase that playback is already at
     * the end of the buffer and pauses immediatly. */
    d_data->m_playbackID = d_data->m_selectedBufferID;
    d_data->m_ui->lblPlaybackBuffer->setText(QString("%1").arg(d_data->m_playbackID));

    // start playback
    beforeCommand();
    emit PlayChanged(d_data->m_selectedBufferID, d_data->m_speed);
    afterCommand();

    // Clear selection of buffer table
    d_data->m_ui->tblBufferStatus->clearSelection();

    // update white balance gains as they were now loaded from the buffer
    emit WbUpdate();
}

/*
 * PlayBackBox::onStopPlaybackClicked
 *****************************************************************************/
void PlayBackBox::onStopPlaybackClicked()
{
    // Emit signal
    beforeCommand();
    emit PlayStopped();
    afterCommand();

    // Clear selection of buffer table
    d_data->m_ui->tblBufferStatus->clearSelection();
}

/*
 * PlayBackBox::onMarkInClicked
 *****************************************************************************/
void PlayBackBox::onMarkInClicked()
{
    // set mark in position
    beforeCommand();
    emit MarkIn(d_data->m_ui->sldPlaybackPosition->GetUpperValue());
    afterCommand();
}

/*
 * PlayBackBox::onMarkOutClicked
 *****************************************************************************/
void PlayBackBox::onMarkOutClicked()
{
    // set mark out position
    beforeCommand();
    emit MarkOut(d_data->m_ui->sldPlaybackPosition->GetUpperValue());
    afterCommand();
}

/*
 * PlayBackBox::onResetMarksClicked
 *****************************************************************************/
void PlayBackBox::onResetMarksClicked()
{
    // reset mark in and mark out position
    beforeCommand();
    emit MarkIn(d_data->m_ui->sldMarkedArea->GetMinimun());
    emit MarkOut(d_data->m_ui->sldMarkedArea->GetMaximun());
    afterCommand();
}

/*
 * PlayBackBox::onPositionSliderMoved
 *****************************************************************************/
void PlayBackBox::onPositionSliderMoved(int position)
{
    // seek to position
    beforeCommand();
    emit SeekChanged(1 /* absolute seek */, position);
    afterCommand();
}

/*
 * PlayBackBox::onMarkInPositionSliderMoved
 *****************************************************************************/
void PlayBackBox::onMarkInPositionSliderMoved(int mark_in)
{
    beforeCommand();

    // pause playback if not already done
    if (!d_data->m_playbackPaused) {
        emit PlayPaused();
        d_data->m_playbackPaused = true;
    }

    // seek to slider position
    emit SeekChanged(1 /* absolute seek */, mark_in);

    // set mark in position
    emit MarkIn(mark_in);

    afterCommand();
}

/*
 * PlayBackBox::onMarkOutPositionSliderMoved
 *****************************************************************************/
void PlayBackBox::onMarkOutPositionSliderMoved(int mark_out)
{
    beforeCommand();

    // pause playback if not already done
    if (!d_data->m_playbackPaused) {
        emit PlayPaused();
        d_data->m_playbackPaused = true;
    }

    // seek to slider position
    emit SeekChanged(1 /* absolute seek */, mark_out);

    // set mark in position
    emit MarkOut(mark_out);

    afterCommand();
}

/*
 * PlayBackBox::onPlaybackSpeedSliderMoved
 *****************************************************************************/
void PlayBackBox::onPlaybackSpeedSliderMoved(int speed)
{
    // store new speed
    d_data->m_speed = speed;

    // seek to position
    beforeCommand();
    emit PlayChanged(d_data->m_playbackID, d_data->m_speed);
    afterCommand();
}

/*
 * PlayBackBox::onPlayClicked
 *****************************************************************************/
void PlayBackBox::onPlayClicked()
{
    // If playback is paused or running in backward mode
    if (d_data->m_playbackPaused) {
        /* if speed is 0 (because speed bar was dragged there) assume that
         * the user wants to play with speed x1 if he clicks the play button */
        if (d_data->m_speed == 0) {
            d_data->m_speed = 10;
        }

        // start playback
        beforeCommand();
        emit PlayChanged(d_data->m_playbackID, d_data->m_speed);
        afterCommand();
    } else {
        // pause playback
        beforeCommand();
        emit PlayPaused();
        afterCommand();
    }
}
