/*
 * Copyright (C) 2017 Dream Chip Technologies GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
/**
 * @file    id.h
 *
 * @brief   Unique camera system identifier (used by GUI)
 *
 *****************************************************************************/
#ifndef ID_H
#define ID_H

enum SystemId {
    SYSTEM_ID_INVALID = -1, // unknown system
    SYSTEM_ID_ATOM_ONE = 0, // AtomOne (xbow)
    SYSTEM_ID_ATOM_ONE_4K = 1, // AtomOne 4K / 4K mini (condor)
    SYSTEM_ID_ATOM_ONE_MINI = 2, // AtomOne mini (cooper)
    SYSTEM_ID_ATOM_ONE_MINI_ZOOM = 3, // AtomOne mini Zoom (blackline)
    SYSTEM_ID_MAX
};

#endif // ID_H
