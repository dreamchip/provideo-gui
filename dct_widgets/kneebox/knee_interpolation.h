/*
 * Copyright (C) 2017 Dream Chip Technologies GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
/**
 * @file    interpolation.h
 *
 * @brief   Class definition of a cubic spline interpolation
 *
 *****************************************************************************/
#ifndef KNEE_INTERPOLATION_H
#define KNEE_INTERPOLATION_H

#include <stdint.h>

#define CFG_WDR_INPUT_WIDTH (12u) /**< input bit-width of wdr module */
#define CFG_WDR_GAIN_COMMA (14u) /**< comma position of wdr gain table  */
#define CFG_WDR_GAIN_MASK (0x0003ffffu) /**< value 18 bit in FP4.14 */

class KneeInterpolation
{
public:
    explicit KneeInterpolation();
    ~KneeInterpolation();

    void clear();
    void setConfig(uint8_t knee, uint16_t slope);
    int interpolate(int x);

private:
    class PrivateData;
    PrivateData *d_data;
};

#endif // KNEE_INTERPOLATION_H
