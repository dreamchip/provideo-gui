#include "infodialog.h"
#include "ui_infodialog.h"

#include <QIcon>
#include <QScreen>
#include <QWindow>
#include <QGuiApplication>

InfoDialog::InfoDialog(QString iconFilePath, QString windowTitle, QString infoText, QWidget *parent)
    : QDialog(parent), ui(new Ui::InfoDialog)
{
    ui->setupUi(this);

    // Show without frame
    this->setWindowFlags(Qt::Dialog | Qt::FramelessWindowHint | Qt::MSWindowsFixedSizeDialogHint);

    // Set Icon and text
    this->setWindowTitle(windowTitle);
    this->setWindowIcon(QIcon(iconFilePath));
    this->ui->lblImage->setPixmap(QPixmap(iconFilePath));
    this->ui->lblInfo->setText(infoText);
}

InfoDialog::~InfoDialog()
{
    delete ui;
}

void InfoDialog::showEvent(QShowEvent *event)
{
    QDialog::showEvent(event);

    // Setup position of the info dialog

    // If there is a parent and the parent is currently visible,
    // show info dialog in middle of parent.
    if (parentWidget() && parentWidget()->isVisible()) {
        auto hostRect = parentWidget()->geometry();
        this->move(hostRect.center() - this->rect().center());
    }
    // Else if there is a parent, but it is not visible, show dialog
    // in the middle of the screen that the parent (should be) visible on.
    else if (parentWidget()) {
        QRect screenGeometry = parentWidget()->windowHandle()->screen()->geometry();
        int x = (screenGeometry.width() - this->width()) / 2;
        int y = (screenGeometry.height() - this->height()) / 2;
        this->move(x, y);
    }
    // If there is no parent, show the dialog in the middle of screen 0.
    else {
        QRect screenGeometry = QGuiApplication::screens().at(0)->geometry();
        int x = (screenGeometry.width() - this->width()) / 2;
        int y = (screenGeometry.height() - this->height()) / 2;
        this->move(x, y);
    }
}
