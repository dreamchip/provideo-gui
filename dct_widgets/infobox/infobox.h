/*
 * Copyright (C) 2017 Dream Chip Technologies GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
/**
 * @file    infobox.h
 *
 * @brief   Class definition of a system information box
 *
 *****************************************************************************/
#ifndef INFO_BOX_H
#define INFO_BOX_H

#include <dct_widgets_base.h>

/*
 * System Information Box Widget
 *****************************************************************************/
class InfoBox : public DctWidgetBox
{
    Q_OBJECT

public:
    explicit InfoBox(QWidget *parent = 0);
    ~InfoBox() Q_DECL_OVERRIDE;

    // Show or hide UI elements
    void setRuntimeVisible(bool value);
    void setFanSettingsVisible(bool value);
    void setNumTempSensors(unsigned int tempSensorCount);
    void setSystemOperationsVisible(bool value);
    void setStorageInfoVisible(bool value);

protected:
    void showEvent(QShowEvent *event) Q_DECL_OVERRIDE;
    void hideEvent(QHideEvent *event) Q_DECL_OVERRIDE;

    void prepareMode(Mode mode) Q_DECL_OVERRIDE;

    void loadSettings(QSettings &s) Q_DECL_OVERRIDE;
    void saveSettings(QSettings &s) Q_DECL_OVERRIDE;
    void applySettings(void) Q_DECL_OVERRIDE;

signals:
    void GetRunTimeRequest();
    void GetTempRequest(uint8_t id);
    void GetMaxTempRequest();
    void GetFanSpeedRequest();
    void FanTargetChanged(uint8_t target);
    void GetOverTempCountRequest();
    void GetHealthRequest();
    void MaxTempReset();
    void Shutdown();
    void Reboot();
    void Update();
    void SwitchModeChanged(uint8_t id);
    void ShowConnectDialog();

public slots:
    // system interface slots
    void onDeviceNameChange(QString name);
    void onSystemPlatformChange(QString platform);
    void onDeviceIdChange(QString id);
    void onSystemValidityChange(QString version);
    void onBitStreamVersionChange(uint32_t version);
    void onApplicationVersionChange(QString version);
    void onApplicationReleaseDateChange(QString date);
    void onApplicationBuildDateChange(QString date);
    void onBootloaderVersionChange(QString version);
    void onFeatureMaskHwChange(uint32_t mask);
    void onFeatureMaskHwListChange(QStringList features);
    void onFeatureMaskSwChange(uint32_t mask);
    void onRunTimeChange(uint32_t seconds);
    void onTempChange(uint8_t id, float temp, QString name);
    void onMaxTempChange(int32_t max_temp_logged_user, int32_t max_temp_logged_persistent,
                         int32_t max_temp_allowed);
    void onFanSpeedChange(uint8_t speed);
    void onFanTargetChange(uint8_t target);
    void onOverTempCountChange(uint32_t count);
    void onSwitchModeChange(uint8_t id);
    void onHealthChange(int ssd_id, int status_code, QString status_string, int temp_min, int temp,
                        int temp_max, int lifetime);

private slots:
    void onUpdateTimerExpired();
    void onSbxFanTargetChanged(int target);
    void onResetMaxTempClicked();
    void onShutdownClicked();
    void onRebootClicked();
    void onUpdateClicked();
    void onSwitchToTriggerModeClicked();
    void onSwitchToSSMModeClicked();
    void onShowLicenseClicked();
    void onShowThirdPartyLicensesClicked();

private:
    class PrivateData;
    PrivateData *d_data;
};

#endif // INFO_BOX_H
