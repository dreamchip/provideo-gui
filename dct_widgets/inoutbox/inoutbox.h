/*
 * Copyright (C) 2017 Dream Chip Technologies GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
/**
 * @file    inoutbox.h
 *
 * @brief   Class definition of input/output box
 *
 *****************************************************************************/
#ifndef IN_OUT_BOX_H
#define IN_OUT_BOX_H

#include <dct_widgets_base.h>

#include <array>

/*
 * global definitions
 *****************************************************************************/
using lsc_setup_t = struct lsc_setup_t
{
    bool enable;
    float k;
    float offset;
    float slope;
};

using aec_setup_t = struct aec_setup_t
{
    bool run;
    int setPoint;
    int speed;
    int ClmTolerance;
    int costGain;
    int costTInt;
    int costAperture;
    int tAf;
    int maxGain;
    bool useCustomWeighting;
    std::array<uint8_t, 25> weights;
};

/*
 * In-/Output Box Widget
 *****************************************************************************/
class InOutBox : public DctWidgetBox
{
    Q_OBJECT

public:
    explicit InOutBox(QWidget *parent = nullptr);
    ~InOutBox() Q_DECL_OVERRIDE;

    int BayerPattern() const;
    void setBayerPattern(int);
    int CameraIso() const;
    void setCameraIso(int value);
    int CameraExposure() const;
    void setCameraExposure(int value);

    bool AecEnable() const;
    void setAecEnable(bool value);
    int AecCostTInt() const;
    void setAecCostTInt(int value);
    int AecCostIris() const;
    void setAecCostIris(int value);
    int AecSetPoint() const;
    void setAecSetPoint(int value);
    int AecMaxIso() const;
    void setAecMaxIso(int value);
    int AecControlSpeed() const;
    void setAecControlSpeed(int value);
    int AecFlickerFrequency() const;
    void setAecFlickerFrequency(int value);
    bool AecCustomWeights() const;
    void setAecCustomWeights(bool value);

    bool LscEnable() const;
    float LscK() const;
    float LscOffset() const;
    float LscSlope() const;
    void setLsc(lsc_setup_t setup);

    QString FpsMode() const;
    void setFpsMode(const QString &mode);
    int Phases() const;
    void setPhases(int phases);
    QString VideoMode() const;
    void setVideoMode(const QString &mode);
    QString Sdi2Mode() const;
    void setSdi2Mode(const QString &mode);
    QString Sdi1Downscaler() const;
    void setSdi1Downscaler(const QString &mode);
    QString Sdi2Downscaler() const;
    void setSdi2Downscaler(const QString &mode);
    QString FlipMode() const;
    void setFlipMode(const QString &mode);
    QString LogMode() const;
    void setLogMode(const QString &mode);
    int PQMaxBrightness() const;
    void setPQMaxBrightness(int value);
    int SLog3MasterGain() const;
    void setSLog3MasterGain(int value);
    QString ColorSpace() const;
    void setColorSpace(const QString &mode);
    bool TestPattern() const;
    void setTestPattern(bool value);
    bool AudioEnable() const;
    void setAudioEnable(bool value);
    double AudioGain() const;
    void setAudioGain(double gain);

    QString GenLockMode() const;
    void setGenLockMode(const QString &mode);
    QString GenLockCrosslockEnable() const;
    QString GenLockCrosslockVmode() const;
    void setGenLockCrosslock(const QString &enable, const QString &vmode);
    int GenLockOffsetVertical() const;
    void setGenLockOffsetVertical(int value);
    int GenLockOffsetHorizontal() const;
    void setGenLockOffsetHorizontal(int value);
    bool GenLockTermination() const;
    void setGenLockTermination(bool value);

    // Show or hide UI elements
    void setCameraSettingsVisible(bool value);
    void setInterlacedSDI5Visible(bool value);
    void setAutoExposureSettingsVisible(bool value);
    void setLensChadingCorrectionSettingsVisible(bool value);
    void setGenLockVisible(bool genlock_visible, bool crosslock_visible);
    void setTimeCodeVisible(bool groupbox_visible, bool hold_visible);
    void setFpsModeVisible(bool value);
    void setPhasesVisible(bool value);
    void setSdi2ModeVisible(bool value);
    void setDownscaleModeVisible(bool value);
    void setFlipModeVisible(bool vertical, bool horizontal);
    void setLogModeVisible(bool value);
    void setTestPatternVisible(bool value);
    void setAudioVisible(bool value);

    void addVideoMode(const QString &name, int id);
    void clearAllVideoModes();

    void addGenlockCrosslockVideoMode(const QString &name, int id);
    void clearAllGenlockCrosslockVideoModes();

protected:
    void prepareMode(Mode mode) Q_DECL_OVERRIDE;

    void loadSettings(QSettings &s) Q_DECL_OVERRIDE;
    void saveSettings(QSettings &s) Q_DECL_OVERRIDE;
    void applySettings() Q_DECL_OVERRIDE;

    void addBayerPattern(const QString &name, int id);
    void addFpsMode(const QString &name, int id);
    void addGenlockMode(const QString &name, int id);
    void addGenlockCrosslockEnable(const QString &name, int id);
    void addSdi2Mode(const QString &name, int id);
    void addDownscaleMode(const QString &name, int id);
    void addFlipMode(const QString &name, int id);
    void addLogMode(const QString &name, int id);
    void addColorSpace(const QString &name, int id);

    void UpdateIsoPlusMinusButtons();
    void UpdateExposurePlusMinusButtons();
    void UpdateIsoComboBox(int iso);
    void UpdateExposureComboBox(int exposure);

    void updateAecSetupWidgets();
    void updateLscWidgets();
    void enableAecWidgets(bool enable);
    void enableCamConfWidgets(bool enable);

    void updateEnableOfGenlockSettings(int genlockMode, int crosslockMode);

    QVector<int> createAecVector();
    QVector<uint> createLscVector();

signals:
    void LscChanged(QVector<uint> values);

    void BayerPatternChanged(int value);

    void CameraGainChanged(int value);
    void CameraExposureChanged(int value);

    void UpdateCameraExposure();

    void ChainFpsModeChanged(int value);
    void ChainPhasesChanged(int value);
    void ChainVideoModeChanged(int mode, bool interlaced_sdi5);
    void ChainSdi2ModeChanged(int value);
    void ChainDownscaleModeChanged(int sdi_out_idx, bool downscale, bool interlace);
    void ChainFlipModeChanged(int value);
    void LogModeChanged(int value);
    void PQMaxBrightnessChanged(int value);
    void SLog3MasterGainChanged(int value);
    void ColorSpaceChanged(int value);
    void OsdTestPatternChanged(int value);
    void ChainAudioEnableChanged(bool enable);
    void ChainAudioGainChanged(double gain);

    void ChainGenlockModeChanged(int value);
    void ChainGenlockCrosslockChanged(int enable, int vmode);
    void ChainGenlockOffsetChanged(int vertical, int horizontal);
    void ChainGenlockTerminationChanged(int value);

    void AecEnableChanged(int value);
    void AecSetupChanged(QVector<int> values);
    void AecSetupChangedForLensDriver(QVector<int> values);
    void AecWeightChanged(int index, int weight);
    void WeightDialogAecWeightsChanged(QVector<int> weights);

    void ChainTimecodeGetRequested();
    void ChainTimecodeSetChanged(QVector<int>);
    void ChainTimecodeHoldChanged(bool);

    void IrisAptChanged(int);

    void ResyncRequest();

public slots:
    void onBayerPatternChange(int value);

    void onCameraInfoChange(int min_gain, int max_gain, int min_exposure, int max_exposure,
                            int min_iso);
    void onCameraGainChange(int value);
    void onCameraGainClipChange(int value);
    void onCameraExposureChange(int value);

    void onChainFpsModeChange(int value);
    void onChainPhasesChange(int value);
    void onChainVideoModeChange(int value, bool interlaced_sdi5);
    void onChainSdi2ModeChange(int value);
    void onChainDownscaleModeChange(int sdi_out_idx, bool downscale, bool interlace);
    void onChainFlipModeChange(int value);
    void onLogModeChange(int value);
    void onPQMaxBrightnessChange(int max_brightness);
    void onSLog3MasterGainChange(int master_gain);
    void onColorSpaceChange(int value);
    void onOsdTestPatternChange(int value);
    void onChainAudioEnableChange(bool enable);
    void onChainAudioGainChange(double gain);

    void onChainGenlockModeChange(int value);
    void onChainGenlockCrosslockChange(int enable, int vmode);
    void onChainGenlockOffsetChange(int vertical, int horizontal);
    void onChainGenlockTerminationChange(int value);

    void onAecEnableChange(int enable);
    void onAecSetupChange(QVector<int> values);
    void onAecWeightsChange(QVector<int> weights);
    void onIrisAvailableChange(bool available);

    void onAecStatChange(QVector<int> values);

    void onLscChange(QVector<uint> values);

    void onChainTimecodeChange(QVector<int> time);
    void onChainTimecodeHoldChange(bool enable);

private slots:
    void onCbxBayerPatternChange(int index);

    void onSldIsoChange(int value);
    void onSldIsoReleased();
    void onSbxIsoChange(int value);
    void onCbxIsoChange(int index);
    void onBtnIsoMinusClicked();
    void onBtnIsoPlusClicked();

    void onSldExposureChange(int value);
    void onSldExposureReleased();
    void onSbxExposureChange(int value);
    void onCbxExposureChange(int index);
    void onBtnExposureMinusClicked();
    void onBtnExposurePlusClicked();

    void onCbxFpsModeChange(int index);
    void onSbxPhasesChange(int value);
    void onCbxVideoModeChange(int index);
    void onCbxInterlacedSDI5Change(int state);
    void onCbxSdi2ModeChange(int index);
    void onCbxSdi1DownscalerChange(int index);
    void onCbxSdi2DownscalerChange(int index);
    void onCbxFlipModeChange(int index);
    void onCbxLogModeChange(int index);
    void onSbxPQMaxBrightnessChange(int value);
    void onSbxSLog3MasterGainChange(int value);
    void onCbxColorSpaceChange(int index);
    void onCbxTestPatternChange(int value);
    void onCbxAudioEnableChange(int value);
    void onSbxAudioGainChange(double gain);

    void onCbxGenlockModeChange(int index);
    void onCbxGenlockCrosslockEnableChange(int index);
    void onCbxGenlockCrosslockVmodeChange(int index);
    void onSbxGenlockOffsetVerticalChange(int value);
    void onSbxGenlockOffsetHorizontalChange(int value);
    void onCbxGenlockTerminationChange(int value);

    void onCbxAecEnableChange(int value);
    void onCbxAecWeightChange(int value);

    void onSldSetPointChange(int value);
    void onSldSetPointReleased();
    void onSbxSetPointChange(int value);

    void onSldMaxIsoChange(int value);
    void onSldMaxIsoReleased();
    void onSbxMaxIsoChange(int value);

    void onSldControlSpeedChange(int value);
    void onSldControlSpeedReleased();
    void onSbxControlSpeedChange(int value);

    void onBtnTimecodeSetClicked();
    void onBtnTimecodeGetClicked();
    void onBtnTimecodeHoldClicked(bool checked);

    void onTaf50Toggle(bool checked);
    void onTaf60Toggle(bool checked);

    void onAecAutoIrisToggle(bool checked);
    void onAecManualIrisToggle(bool checked);
    void onAecManualShutterToggle(bool checked);

    void onCbxLscEnableChange(int value);

    void onSldKChange(int value);
    void onSldKReleased();
    void onSbxKChange(double value);

    void onSldOffsetChange(int value);
    void onSldOffsetReleased();
    void onSbxOffsetChange(double value);

    void onSldSlopeChange(int value);
    void onSldSlopeReleased();
    void onSbxSlopeChange(double value);

private: // NOLINT(readability-redundant-access-specifiers)
    class PrivateData;
    PrivateData *d_data;

    int gainToIso(int gain) const;
    int isoToGain(int iso) const;
    void checkGainClip(int newGain);
    void show4kGenlockNote(int mode);
    void EmitDownscaleChanged(int sdi_out_idx, int combo_box_idx);
};

#endif // IN_OUT_BOX_H
